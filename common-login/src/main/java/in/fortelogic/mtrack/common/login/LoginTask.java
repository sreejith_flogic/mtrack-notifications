package in.fortelogic.mtrack.common.login;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

import ch.bullfin.httpmanager.HTTPManager;
import ch.bullfin.httpmanager.Response;
import in.fortelogic.mtrack.common.base.MTrackUtils;

/**
 * @author Rahul Raveendran V P
 * Created on 07/04/18 @ 12:39 PM
 * https://github.com/rahulrvp
 */


public class LoginTask extends AsyncTask<Context, Void, User> {
    @SuppressWarnings("FieldCanBeLocal")
    private final String LOG_TAG = "LoginTask";
    private final String mLoginURL;
    private final String mUsername;
    private final String mPassword;
    private final String mInterfaceName;
    private final Callback mCallback;
    private int mErrorCode;

    LoginTask(@NotNull String loginURL, @NotNull String username,
              @NotNull String password, @NotNull String interfaceName, Callback callback) {
        mErrorCode = 0;
        mLoginURL = loginURL;
        mUsername = username;
        mPassword = password;
        mInterfaceName = interfaceName;
        mCallback = callback;
    }

    @Override
    protected void onPreExecute() {
        if (mCallback != null) {
            mCallback.onLoginStart();
        }
    }

    @Override
    protected User doInBackground(Context... contexts) {
        User user = null;

        HashMap<String, Object> params = new HashMap<>();
        params.put("username", mUsername);
        params.put("password", mPassword);
        params.put("interface", mInterfaceName);

        if (contexts != null && contexts.length > 0) {
            params.put("app_name", MTrackUtils.getAppName(contexts[0]));
            params.put("app_version", MTrackUtils.getAppVersion(contexts[0]));
            params.put("app_version_code", MTrackUtils.getAppVersionCode(contexts[0]));
        }

        //TODO Remove the test code
//        HTTPManager httpManager = new HTTPManager(mLoginURL);
//        Response response = httpManager.post(new Gson().toJson(params));
//
//        if (response.getStatusCode() == 200) {
//            try {
//                user = new Gson().fromJson(response.getResponseBody(), User.class);
//                if (user == null || user.getUserToken() == null) {
//                    // invalid user
//                    user = null;
//                }
//            } catch (Exception e) {
//                Log.e(LOG_TAG, e.getMessage());
//            }
//        } else {
//            mErrorCode = response.getStatusCode();
//        }
        user = User.getInstance(contexts[0]);
        user.setUsername(mUsername);

        return user;
    }

    @Override
    protected void onPostExecute(User user) {
        if (mCallback != null) {
            if (user != null) {
                // success
                mCallback.onLoginSuccess(user);
            } else {
                // failure
                mCallback.onLoginFailure(mErrorCode);
            }
        }
    }

    @Override
    protected void onCancelled() {
        if (mCallback != null) {
            mCallback.onLoginFailure(mErrorCode);
        }
    }

    public interface Callback {
        void onLoginStart();

        void onLoginSuccess(User user);

        void onLoginFailure(int errorCode);
    }
}
