package in.fortelogic.mtrack.common.login;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.io.Serializable;
import java.util.HashMap;

import in.fortelogic.mtrack.common.base.MTrackUtils;

public class User implements Serializable {
    public static final String PREF_FILE = "session";
    private static final String PREF_KEY_STAFF = "user";

    private static User _instance;

    private String username;
    private String name;
    private String user_token;

    private User() {}

    public static User getInstance(Context context) {
        if (_instance == null) {
            SharedPreferences store = context.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE);
            if (store != null) {
                String json = store.getString(PREF_KEY_STAFF, null);
                if (json != null) {
                    _instance = new Gson().fromJson(json, User.class);
                } else {
                    _instance = new User();
                }
            }
        }

        return _instance;
    }

    @SuppressLint("ApplySharedPref")
    public void save(Context context) {
        _instance = this;
        SharedPreferences store = context.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE);
        if (store != null) {
            store.edit().putString(PREF_KEY_STAFF, new Gson().toJson(this)).commit();
        }
    }

    public static void logout(Context context) {
        _instance = new User();
        _instance.save(context);
    }

    public boolean isLoggedIn() {
        return user_token != null && !user_token.isEmpty();
    }

    public static boolean isLoggedIn(Context context) {
        User user = getInstance(context);
        return user != null && user.user_token != null && !user.user_token.isEmpty();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public String getUserToken() {
        return user_token;
    }

    public static HashMap<String, Object> getAuthParams(Context context) {
        HashMap<String, Object> params = new HashMap<>();
        User user = User.getInstance(context);
        if (user.isLoggedIn()) {
            params.put("username", user.getUsername());
            params.put("user_token", user.getUserToken());

            // extra params
//            params.put("interface", MTrackUtils.getSavedInterfaceName(context));
            params.put("app_name", MTrackUtils.getAppName(context));
            params.put("app_version", MTrackUtils.getAppVersion(context));
            params.put("app_version_code", MTrackUtils.getAppVersionCode(context));
        }

        return params;
    }
}
