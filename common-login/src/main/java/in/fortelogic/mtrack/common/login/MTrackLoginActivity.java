package in.fortelogic.mtrack.common.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.github.rahulrvp.android_utils.BaseFont;
import com.github.rahulrvp.android_utils.EditTextUtils;
import com.google.android.material.button.MaterialButton;

import org.jetbrains.annotations.NotNull;

import in.fortelogic.mtrack.common.base.MTrackUtils;
import in.fortelogic.mtrack.common.ui.ESMedium;

import static in.fortelogic.mtrack.common.login.BuildConfig.BASE_URL;

public abstract class MTrackLoginActivity extends AppCompatActivity {

    public Context mContext;
    private LoginTask mAuthTask = null;
    private EditText mPasswordField;
    private EditText mEmailField;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_login);

        mContext = this;

        if (User.isLoggedIn(mContext)) {
            onLoggedIn();
            finish();
        }

        mEmailField = findViewById(R.id.email_field);
        mPasswordField = findViewById(R.id.password_field);
        mPasswordField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }

                return false;
            }
        });

        Button loginBtn = findViewById(R.id.sign_in_button);
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptLogin();
            }
        });

        // apply common font
        BaseFont baseFont = getCommonFont();
        if (baseFont != null) {
            baseFont.apply(findViewById(android.R.id.content));
        }
    }

    /**
     * This method is responsible for providing the font object.
     * Override this to return your own font object.
     *
     * @return BaseFont instance.
     */
    public BaseFont getCommonFont() {
        return ESMedium.getInstance();
    }

    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        String email = EditTextUtils.getText(mEmailField);
        String password = EditTextUtils.getText(mPasswordField);

        if (! MTrackUtils.isValidEmailAddress(email)) {
            mEmailField.setError(getString(R.string.error_invalid_email));
            mEmailField.requestFocus();
            return;
        }

        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordField.setError(getString(R.string.error_invalid_password));
            mPasswordField.requestFocus();
            return;
        }

        mAuthTask = createLoginTask(email, password);
        mAuthTask.execute(mContext);
    }

    private LoginTask createLoginTask(String username, String password) {
        return new LoginTask(
                getLoginURL(),
                username,
                password,
                getInterfaceName(),
                new LoginTask.Callback() {
                    @Override
                    public void onLoginStart() {
                        onLoginBegin();
                    }

                    @Override
                    public void onLoginSuccess(User user) {
                        mAuthTask = null;

                        MTrackUtils.saveInterfaceName(mContext, getInterfaceName());
                        user.save(mContext);

                        onLoginEnd();
                    }

                    @Override
                    public void onLoginFailure(int errorCode) {
                        mAuthTask = null;

                        hideProgress();

                        if (errorCode == 401) {
                            mPasswordField.setError(getString(R.string.error_incorrect_password_or_username));
                            mPasswordField.requestFocus();
                        } else if (errorCode == 403) {
                            showPermissionDeniedMessage();
                        }
                    }
                });
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }

    public void showProgress() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(mContext);
        }

        mProgressDialog.setMessage(getString(R.string.loading_pls_wait));
        mProgressDialog.setCancelable(false);

        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    public void hideProgress() {
        if (mProgressDialog != null && mProgressDialog.isShowing() && !isFinishing()) {
            try {
                mProgressDialog.dismiss();
            } catch (Exception ignored) {
            }
        }
    }

    protected void onLoginBegin() {
        // implement this in child class to show progress
    }

    protected void onLoginEnd() {
        // implement this in child class to navigate to next page
    }

    protected void onLoggedIn() {
        // implement this in child class to navigate to next page
        launchNextPage(getNextPage());
    }

    protected void showPermissionDeniedMessage() {
        new AlertDialog.Builder(mContext, R.style.dialogTheme)
                .setTitle(R.string.dlg_no_permission_title)
                .setMessage(R.string.dlg_no_permission_message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    /**
     * Override this method to privide if the user wants to provide a
     * different login URL.
     *
     * @return valid login API url.
     */
    @NotNull
    protected String getLoginURL() {
        return BASE_URL + "/users/login.json";
    }

    protected void launchNextPage() {
        launchNextPage(getNextPage());
    }

    private void launchNextPage(Class clazz) {
        if (clazz != null) {
            Intent intent = new Intent(mContext, clazz);
            startActivity(intent);

            finish();
        }
    }

    protected abstract Class getNextPage();

    protected abstract String getInterfaceName();
}
