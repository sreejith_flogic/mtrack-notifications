package in.fortelogic.mtrack.common.validation;

import android.content.Context;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.Log;
import android.widget.EditText;
import android.widget.Spinner;

import com.github.rahulrvp.android_utils.EditTextUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import in.fortelogic.mtrack.common.R;
import in.fortelogic.mtrack.common.dialog.SnackBarDialog;

public class ValidationUtils {
    private static final String LOG_TAG = "ValidationUtils";

    private static boolean validateWithRegex(Validation validation, EditText editText) {
        try {
            String value = EditTextUtils.getText(editText);
            boolean isValid = isInputValidAgainstGivenRegx(
                    editText.getContext(), validation, value, false);

            if (!isValid) {
                EditTextUtils.setError(editText, R.string.error_invalid_input);
            }

            return isValid;
        } catch (Exception e) {
            Log.d(LOG_TAG, e.getMessage());
            EditTextUtils.setError(editText, R.string.error_invalid_input);
            return false;
        }
    }

    private static boolean validateForRequiredField(Validation validation, EditText editText) {
        String value = EditTextUtils.getText(editText);
        if (validation.isRequired()) {
            if (value == null || TextUtils.isEmpty(value.trim())) {
                EditTextUtils.setError(editText, R.string.error_field_required);
                return false;
            }
        }
        return true;
    }

    private static boolean validateForMaxValue(Validation validation, EditText editText) {
        try {
            String value = EditTextUtils.getText(editText);
            boolean isValid = isInputValidAgainstGivenMaxValue(
                    editText.getContext(), validation, value, false);

            if (!isValid) {
                EditTextUtils.setError(
                        editText, R.string.max_limit_error, validation.getMax());
            }

            return isValid;
        } catch (Exception e) {
            Log.d(LOG_TAG, e.getMessage());
            EditTextUtils.setError(editText, R.string.error_invalid_input);
            return false;
        }
    }

    private static boolean validateForMinValue(Validation validation, EditText editText) {
        try {
            String value = EditTextUtils.getText(editText);
            boolean isValid = isInputValidAgainstGivenMinValue(
                    editText.getContext(), validation, value, false);

            if (!isValid) {
                EditTextUtils.setError(
                        editText, R.string.min_limit_error, validation.getMin());
            }

            return isValid;
        } catch (Exception e) {
            Log.d(LOG_TAG, e.getMessage());
            EditTextUtils.setError(editText, R.string.error_invalid_input);
            return false;
        }
    }

    private static boolean validateForMinLengthValue(Validation validation, EditText editText) {
        try {
            String value = EditTextUtils.getText(editText);
            boolean isValid = isInputValidAgainstGivenMinLength(
                    editText.getContext(), validation, value, false);

            if (!isValid) {
                EditTextUtils.setError(
                        editText, R.string.min_length_error, validation.getMinLength());
            }

            return isValid;
        } catch (Exception e) {
            Log.d(LOG_TAG, e.getMessage());
            EditTextUtils.setError(editText, R.string.error_invalid_input);
            return false;
        }
    }

    private static boolean validateForMaxLengthValue(Validation validation, EditText editText) {
        try {
            String value = EditTextUtils.getText(editText);
            boolean isValid = isInputValidAgainstGivenMaxLength(
                    editText.getContext(), validation, value, false);

            if (!isValid) {
                EditTextUtils.setError(
                        editText, R.string.max_length_error, validation.getMaxLength());
            }

            return isValid;
        } catch (Exception e) {
            Log.d(LOG_TAG, e.getMessage());
            EditTextUtils.setError(editText, R.string.error_invalid_input);
            return false;
        }
    }

    public static boolean shouldValidate(Context context, String validationKey) {
        Validation validation = ValidationTable.getInstance(context).getValidation(validationKey);
        return validation != null;
    }

    public static void prepareEditText(EditText editText, String validationKey) {
        if (editText != null) {
            Validation validation =
                    ValidationTable.getInstance(editText.getContext()).getValidation(validationKey);

            if (validation != null) {
                List<InputFilter> inputFilterList = new ArrayList<>();

                if (validation.getDefaultValue() != null) {
                    String currentVal = EditTextUtils.getText(editText);
                    if (TextUtils.isEmpty(currentVal)) {
                        editText.setText(validation.getDefaultValue());
                    } else {
                        Log.d(LOG_TAG, "Skipping default value as value is already preset");
                    }
                }

                if (!TextUtils.isEmpty(validation.getRegx())) {
                    inputFilterList.add(new RegxInputFilter(validation.getRegx()));
                }

                if (ValidationType.ALPHA_NUMERIC.equals(validation.getType())) {
                    inputFilterList.add(new RegxInputFilter("^[a-zA-Z0-9_]*$"));
                }

                if (validation.isMaxLengthCheckApplicable()) {
                    inputFilterList.add(new InputFilter.LengthFilter(validation.getMaxLength()));
                }

                InputFilter[] filters = new InputFilter[inputFilterList.size()];
                int index = 0;
                for (InputFilter filter : inputFilterList) {
                    filters[index++] = filter;
                }

                // set input filter
                editText.setFilters(filters);

                // set input type
                editText.setInputType(validation.getInputType());
            }
        }
    }

    public static boolean validate(EditText editText, String validationKey) {
        if (editText != null) {
            Context context = editText.getContext();
            Validation validation = ValidationTable.getInstance(context).getValidation(validationKey);
            if (validation != null) {
                return applyValidation(validation, editText);
            }

            return true;
        }

        return false;
    }

    public static boolean validate(Spinner spinner, String validationKey, boolean showError) {
        if (spinner != null) {
            Context context = spinner.getContext();
            Validation validation = ValidationTable.getInstance(context).getValidation(validationKey);
            if (validation != null) {
                String spinnerValue =
                        spinner.getSelectedItem() != null ? spinner.getSelectedItem().toString() : null;
                return validate(context, validationKey, spinnerValue, showError);
            }

            return true;
        }

        return false;
    }

    private static boolean applyValidation(Validation validation, EditText editText) {
        return validateForMaxLengthValue(validation, editText)
                && validateForRequiredField(validation, editText)
                && validateForMinLengthValue(validation, editText)
                && validateForMaxValue(validation, editText)
                && validateForMinValue(validation, editText)
                && validateWithRegex(validation, editText);
    }

    // for list
    public static boolean validateAttachments(
            Context context, String validationKey, List attachmentList, boolean showError) {
        if (context != null && attachmentList != null) {
            Validation validation = ValidationTable.getInstance(context).getValidation(validationKey);
            if (validation != null) {
                // apply validation on length only.
                return applyValidation(context, validation, attachmentList, showError);
            }

            return true;
        }

        return false;
    }

    private static boolean applyValidation(Context context, Validation validation, List input, boolean showError) {
        return isInputValidAgainstGivenMaxLength(context, validation, input, showError)
                && isInputValidAgainstGivenMinLength(context, validation, input, showError);
    }

    private static boolean isInputValidAgainstGivenMaxLength(
            Context context, Validation validation, List input, boolean showError) {

        try {
            if (validation.isMaxLengthCheckApplicable()) {
                int length = input.size();
                boolean isValid = validation.isValidMaxLength(length);
                if (!isValid && showError) {
                    String msg = context.getString(
                            R.string.attachments_max_length_error, validation.getMaxLength());
                    new SnackBarDialog(context, msg).show();
                }

                return isValid;
            } else return true;
        } catch (Exception e) {
            if (showError) {
                new SnackBarDialog(context, R.string.error_invalid_input).show();
            }

            return false;
        }
    }

    private static boolean isInputValidAgainstGivenMinLength(
            Context context, Validation validation, List input, boolean showError) {

        try {
            if (validation.isMinLengthCheckApplicable()) {
                int length = input.size();
                boolean isValid = validation.isValidMinLength(length);
                if (!isValid && showError) {
                    String msg = context.getString(
                            R.string.attachments_min_length_error, validation.getMinLength());
                    new SnackBarDialog(context, msg).show();
                }

                return isValid;
            } else return true;
        } catch (Exception e) {
            if (showError) {
                new SnackBarDialog(context, R.string.error_invalid_input).show();
            }

            return false;
        }
    }

    // for value input.
    public static boolean validate(
            Context context, String validationKey, String input, boolean showError) {
        if (context != null && input != null) {
            Validation validation = ValidationTable.getInstance(context).getValidation(validationKey);
            if (validation != null) {
                return applyValidation(context, validation, input, showError);
            }

            return true;
        }

        return false;
    }

    private static boolean applyValidation(
            Context context, Validation validation, String input, boolean showError) {

        return isInputValidAgainstGivenRegx(context, validation, input, showError)
                && validateForRequiredField(context, validation, input, showError)
                && isInputValidAgainstGivenMaxValue(context, validation, input, showError)
                && isInputValidAgainstGivenMinValue(context, validation, input, showError)
                && isInputValidAgainstGivenMaxLength(context, validation, input, showError)
                && isInputValidAgainstGivenMinLength(context, validation, input, showError);
    }

    private static boolean isInputValidAgainstGivenRegx(
            Context context, Validation validation, String input, boolean showError) {

        try {
            if (validation.isRegxCheckApplicable()) {
                Pattern pattern = Pattern.compile(validation.getRegx());
                boolean matches = pattern.matcher(input).matches();

                if (!matches && showError) {
                    new SnackBarDialog(context, R.string.error_invalid_input).show();
                }

                return matches;

            } else return true;
        } catch (Exception e) {
            if (showError) {
                new SnackBarDialog(context, R.string.error_invalid_input).show();
            }

            return false;
        }
    }

    private static boolean validateForRequiredField(
            Context context, Validation validation, String input, boolean showError) {
        if (validation.isRequired()) {
            if (input == null || TextUtils.isEmpty(input.trim())) {
                if (showError) {
                    new SnackBarDialog(context, R.string.error_field_required).show();
                }
                return false;
            }
        }
        return true;
    }

    private static boolean isInputValidAgainstGivenMaxValue(
            Context context, Validation validation, String input, boolean showError) {

        try {
            if (validation.isMaxValueCheckApplicable()) {
                double value = Double.valueOf(input);
                boolean isValid = validation.isValidMaxValue(value);
                if (!isValid && showError) {
                    String msg = context.getString(R.string.max_limit_error, validation.getMax());
                    new SnackBarDialog(context, msg).show();
                }

                return isValid;
            } else return true;
        } catch (Exception e) {
            if (showError) {
                new SnackBarDialog(context, R.string.error_invalid_input).show();
            }

            return false;
        }
    }

    private static boolean isInputValidAgainstGivenMinValue(
            Context context, Validation validation, String input, boolean showError) {

        try {
            if (validation.isMinValueCheckApplicable()) {
                double value = Double.valueOf(input);
                boolean isValid = validation.isValidMinValue(value);
                if (!isValid && showError) {
                    String msg = context.getString(R.string.min_limit_error, validation.getMin());
                    new SnackBarDialog(context, msg).show();
                }

                return isValid;

            } else return true;
        } catch (Exception e) {
            if (showError) {
                new SnackBarDialog(context, R.string.error_invalid_input).show();
            }

            return false;
        }
    }

    private static boolean isInputValidAgainstGivenMaxLength(
            Context context, Validation validation, String input, boolean showError) {

        try {
            if (validation.isMaxLengthCheckApplicable()) {
                int length = input.length();
                boolean isValid = validation.isValidMaxLength(length);
                if (!isValid && showError) {
                    String msg = context.getString(
                            R.string.max_length_error, validation.getMaxLength());
                    new SnackBarDialog(context, msg).show();
                }

                return isValid;
            } else return true;
        } catch (Exception e) {
            if (showError) {
                new SnackBarDialog(context, R.string.error_invalid_input).show();
            }

            return false;
        }
    }

    private static boolean isInputValidAgainstGivenMinLength(
            Context context, Validation validation, String input, boolean showError) {

        try {
            if (validation.isMinLengthCheckApplicable()) {
                int length = input.length();
                boolean isValid = validation.isValidMinLength(length);
                if (!isValid && showError) {
                    String msg = context.getString(
                            R.string.min_length_error, validation.getMinLength());
                    new SnackBarDialog(context, msg).show();
                }

                return isValid;
            } else return true;
        } catch (Exception e) {
            if (showError) {
                new SnackBarDialog(context, R.string.error_invalid_input).show();
            }

            return false;
        }
    }

    public static boolean isAlphaNumeric(String input) {
        Pattern pattern = Pattern.compile("^[a-zA-Z0-9_]*$");
        return !TextUtils.isEmpty(input) && pattern.matcher(input).matches();
    }
}