package in.fortelogic.mtrack.common.base;

import android.app.Application;

import com.firebase.jobdispatcher.Constraint;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.JobService;
import com.firebase.jobdispatcher.Lifetime;
import com.firebase.jobdispatcher.RetryStrategy;

public abstract class MTrackApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        FirebaseJobDispatcher dispatcher =
                new FirebaseJobDispatcher(new GooglePlayDriver(getApplicationContext()));

        Job job = dispatcher
                .newJobBuilder()
                .setService(getJobService())
                .setTag(getJobTag())
                .setLifetime(Lifetime.FOREVER)
                .setRetryStrategy(RetryStrategy.DEFAULT_LINEAR)
                .setConstraints(Constraint.ON_ANY_NETWORK)
                .build();

        dispatcher.mustSchedule(job);
    }

    protected abstract String getJobTag();

    protected abstract Class<? extends JobService> getJobService();
}
