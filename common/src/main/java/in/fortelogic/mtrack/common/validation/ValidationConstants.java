package in.fortelogic.mtrack.common.validation;

public class ValidationConstants {
    public static final double NOT_APPLICABLE_MIN_MAX_VALUE = -909090;
    public static final double NOT_APPLICABLE_MIN_MAX_LENGTH_VALUE = -909090;
}
