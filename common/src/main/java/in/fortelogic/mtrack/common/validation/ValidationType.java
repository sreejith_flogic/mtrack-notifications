package in.fortelogic.mtrack.common.validation;

public class ValidationType {
    public static final String STRING = "STRING";
    public static final String INTEGER = "INTEGER";
    public static final String DECIMAL = "DECIMAL";
    public static final String ALPHA_NUMERIC = "ALPHA_NUMERIC";
}
