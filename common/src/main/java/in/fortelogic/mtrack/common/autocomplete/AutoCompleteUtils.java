package in.fortelogic.mtrack.common.autocomplete;

import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;

import in.fortelogic.mtrack.common.base.BaseAutoCompleteAdapter;

public class AutoCompleteUtils {

    public static void initialize(
            AutoCompleteTextView view,
            BaseAutoCompleteAdapter adapter, int threshold,
            AdapterView.OnItemClickListener listener) {

        if (view != null) {
            view.setThreshold(threshold);
            view.setAdapter(adapter);
            if (listener != null) {
                view.setOnItemClickListener(listener);
            }
        }
    }

}
