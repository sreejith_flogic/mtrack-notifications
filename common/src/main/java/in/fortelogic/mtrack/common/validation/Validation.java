package in.fortelogic.mtrack.common.validation;

import android.content.Context;
import android.text.InputType;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import in.fortelogic.mtrack.common.base.BaseCacheModel;
import in.fortelogic.mtrack.common.base.network.GetApiBaseModel;
import in.fortelogic.mtrack.common.constants.WebserviceURLs;
import in.fortelogic.mtrack.common.login.User;

public class Validation extends BaseCacheModel implements GetApiBaseModel<Validation> {
    public static final String TYPE_COIL_RECEIVE = "COILRECEIVE";
    public static final String TYPE_SLITTING = "SLITTING";
    public static final String TYPE_PRODUCTION = "PRODUCTION";
    public static final String TYPE_TAGGING = "TAGGING";
    public static final String TYPE_MEASUREMENT = "MEASUREMENT";
    public static final String TYPE_QA = "QA";
    public static final String TYPE_DISPATCH = "DISPATCH";
    public static final String TYPE_INFO = "INFO";
    public static final String TYPE_REBUNDLING = "REBUNDLING";

    private Context context;
    private long id;
    private String key;
    private double min;
    private double max;
    private int min_length;
    private int max_length;
    private String type;
    private String regx;
    private String category;
    private String default_value;
    private boolean required;

    public Validation() {
    }

    public Validation(Context context) {
        this.context = context;
    }

    public Validation(Context context, String category) {
        this.context = context;
        this.category = category;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public double getMin() {
        return min;
    }

    public void setMin(double min) {
        this.min = min;
    }

    public double getMax() {
        return max;
    }

    public void setMax(double max) {
        this.max = max;
    }

    public int getMinLength() {
        return min_length;
    }

    public void setMinLength(int min_length) {
        this.min_length = min_length;
    }

    public int getMaxLength() {
        return max_length;
    }

    public void setMaxLength(int max_length) {
        this.max_length = max_length;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getInputType() {
        if (type != null) {
            switch (type) {
                case ValidationType.STRING:
                    return InputType.TYPE_CLASS_TEXT;

                case ValidationType.INTEGER:
                    return InputType.TYPE_CLASS_NUMBER;

                case ValidationType.DECIMAL:
                    return InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL;

                default:
                    return InputType.TYPE_CLASS_TEXT;
            }
        }

        return InputType.TYPE_CLASS_TEXT;
    }

    @Override
    public String getURL() {
        return WebserviceURLs.GET_VALIDATIONS_URL;
    }

    @Override
    public HashMap<String, Object> getArguments() {
        HashMap<String, Object> xtraParams = User.getAuthParams(context);
        if (xtraParams != null && category != null) {
            xtraParams.put("category", category);
            xtraParams.put("interface", category);
        }

        return xtraParams;
    }

    @Override
    public List<Validation> getList(String json) {
        List<Validation> validationList = new ArrayList<>();
        if (!TextUtils.isEmpty(json)) {
            try {
                Type type = new TypeToken<List<Validation>>() {
                }.getType();
                validationList = new Gson().fromJson(json, type);
                ValidationTable.getInstance(this.context).deleteAll();

                for (Validation validation : validationList) {
                    if (validation != null) {
                        ValidationTable.getInstance(this.context).insertOrUpdate(validation);
                    }
                }
            } catch (Exception ignore) {
            }
        }

        return validationList;
    }

    public boolean isMaxValueCheckApplicable() {
        return getMax() != ValidationConstants.NOT_APPLICABLE_MIN_MAX_VALUE;
    }

    public boolean isMinValueCheckApplicable() {
        return getMin() != ValidationConstants.NOT_APPLICABLE_MIN_MAX_VALUE;
    }

    public boolean isMaxLengthCheckApplicable() {
        return getMaxLength() != ValidationConstants.NOT_APPLICABLE_MIN_MAX_LENGTH_VALUE;
    }

    public boolean isMinLengthCheckApplicable() {
        return getMinLength() != ValidationConstants.NOT_APPLICABLE_MIN_MAX_LENGTH_VALUE;
    }

    public boolean isRegxCheckApplicable() {
        return !TextUtils.isEmpty(regx);
    }

    public boolean isValidMinValue(double minValue) {
        return isMinValueCheckApplicable() && getMin() <= minValue;
    }

    public boolean isValidMaxValue(double maxValue) {
        return isMaxValueCheckApplicable() && getMax() >= maxValue;
    }

    public boolean isValidMinLength(int minLength) {
        return isMinLengthCheckApplicable() && getMinLength() <= minLength;
    }

    public boolean isValidMaxLength(int maxLength) {
        return isMaxLengthCheckApplicable() && getMaxLength() >= maxLength;
    }

    public String getRegx() {
        return regx;
    }

    public void setRegx(String regx) {
        this.regx = regx;
    }

    public String getDefaultValue() {
        return default_value;
    }

    public void setDefaultValue(String default_value) {
        this.default_value = default_value;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }
}
