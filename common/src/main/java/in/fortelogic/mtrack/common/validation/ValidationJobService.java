package in.fortelogic.mtrack.common.validation;

import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;

import java.util.List;

import in.fortelogic.mtrack.common.base.network.GetListAsyncTask;

public class ValidationJobService extends JobService {

    @Override
    public boolean onStartJob(final JobParameters job) {

        new GetListAsyncTask<Validation>().setCallback(new GetListAsyncTask.Callback() {
            @Override
            public void onTaskStart() {
                // do nothing
            }

            @Override
            public void onTaskComplete(List results) {
                jobFinished(job, false);
            }
        }).execute(new Validation(this));

        return false;
    }

    @Override
    public boolean onStopJob(JobParameters job) {
        return false;
    }
}
