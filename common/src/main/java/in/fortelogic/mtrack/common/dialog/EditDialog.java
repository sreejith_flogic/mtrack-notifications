package in.fortelogic.mtrack.common.dialog;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import com.github.rahulrvp.android_utils.EditTextUtils;
import com.github.rahulrvp.android_utils.TextViewUtils;

import in.fortelogic.mtrack.common.R;
import in.fortelogic.mtrack.common.ui.ESMedium;

public class EditDialog extends DialogFragment {

    private static final String CURRENT_VALUE_EXTRA = "current_value";
    private static final String TITLE_EXTRA = "title";
    private static final String EXTRA_HINT = "hint";
    private EditText mEditText;
    private Callback mCallback;

    public static EditDialog newInstance(String title, String currentValue) {
        EditDialog fragment = new EditDialog();

        Bundle args = new Bundle();
        args.putString(TITLE_EXTRA, title);
        args.putString(CURRENT_VALUE_EXTRA, currentValue);
        fragment.setArguments(args);

        return fragment;
    }

    public static EditDialog newInstance(String title, String hint, String currentValue) {
        EditDialog fragment = new EditDialog();

        Bundle args = new Bundle();
        args.putString(TITLE_EXTRA, title);
        args.putString(CURRENT_VALUE_EXTRA, currentValue);
        args.putString(EXTRA_HINT, hint);
        fragment.setArguments(args);

        return fragment;
    }

    public EditDialog setCallback(Callback callback) {
        mCallback = callback;
        return this;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        @SuppressLint("InflateParams") View rootView =
                inflater.inflate(R.layout.dialog_edit, null, false);

        ESMedium.getInstance().apply(rootView);

        String currentValue = getArguments().getString(CURRENT_VALUE_EXTRA);
        String title = getArguments().getString(TITLE_EXTRA);
        String hint = getArguments().getString(EXTRA_HINT);

        TextView titleTV = rootView.findViewById(R.id.title_text_view);
        TextView hintTV = rootView.findViewById(R.id.hint_tv);
        mEditText = rootView.findViewById(R.id.weight_edit_text);
        Button submitButton = rootView.findViewById(R.id.dialog_apply_button);

        TextViewUtils.setText(titleTV, title);

        if (! TextUtils.isEmpty(currentValue)) {
            TextViewUtils.setText(mEditText, currentValue);
        }

        if (! TextUtils.isEmpty(hint)) {
            TextViewUtils.setText(hintTV, hint);
        } else {
            TextViewUtils.setText(hintTV, "");
        }

        if (submitButton != null) {
            submitButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mCallback != null && mEditText != null) {

                        String weight = EditTextUtils.getText(mEditText);
                        if (TextUtils.isEmpty(weight)) {
                            mEditText.setError("Invalid input");
                            return;
                        }

                        mCallback.onWeightEdited(weight);
                    }

                    dismiss();
                }
            });
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(rootView);

        return builder.create();
    }

    public interface Callback {
        void onWeightEdited(String newWeight);
    }
}
