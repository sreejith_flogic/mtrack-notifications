package in.fortelogic.mtrack.common.validation;

public class ValidationKey {
    public static final String AWAIL_REBUNDLE_BARCODE = "rb_barcode";
    public static final String AWAIL_REBUNDLE_NEW_BUNDLE_SIZE = "rb_new_bundle_size";
    public static final String AWAIL_REBUNDLE_NEW_BUNDLE_NUMBERS = "rb_new_bundle_numbers";
    public static final String AWAIL_REBUNDLE_NEW_BUNDLE_LENGTH = "rb_new_bundle_length";
    public static final String AWAIL_REBUNDLE_NEW_BUNDLE_THICKNESS = "rb_new_bundle_thickness";
    public static final String AWAIL_REBUNDLE_NEW_BUNDLE_WEIGHT = "rb_new_bundle_weight";
    public static final String AWAIL_REBUNDLE_SPLIT_NOS = "rb_split_numbers";
    public static final String AWAIL_REBUNDLE_SPLIT_WEIGHT = "rb_split_weight";
    public static final String AWAIL_REBUNDLE_ADD_BUNDLE_NOS = "rb_add_bundle_numbers";
    public static final String AWAIL_REBUNDLE_ADD_BUNDLE_WEIGHT = "rb_add_bundle_weight";
    public static final String AWAIL_MEASUREMENT_BARCODE = "msr_barcode";
    public static final String AWAIL_MEASUREMENT_ATTACHMENTS= "msr_attachments";
    public static final String AWAIL_MEASUREMENT_POSITION = "msr_position";
    public static final String AWAIL_MEASUREMENT_THICKNESS = "msr_thickness";
    public static final String AWAIL_MEASUREMENT_WIDTH = "msr_width";
    public static final String AWAIL_COIL_BARCODE = "slt_coil_barcode";
    public static final String AWAIL_COIL_WIDTH = "slt_coil_width";
    public static final String AWAIL_SLIT_WIDTH = "slt_slit_width";
    public static final String AWAIL_SLIT_ATTACHMENTS = "slt_attachments";
    public static final String AWAIL_SLIT_WIDTH_FIELD = "slt_slit_width_field";
    public static final String AWAIL_SLIT_COUNT = "slt_slit_count";
    public static final String AWAIL_SLIT_NET_WEIGHT = "slt_slit_net_weight";
    public static final String AWAIL_UPDATE_SLIT_REMARKS = "slt_update_slit_remark";
    public static final String AWAIL_ADD_COIL_REMARKS = "slt_add_coil_remark";
    public static final String AWAIL_COIL_NET_WEIGHT = "slt_coil_net_weight";
    public static final String AWAIL_OPERATOR_NAME = "slt_operator_name";
    public static final String AWAIL_DISPATCH_SO_NUMBER = "dis_so_number";
    public static final String AWAIL_DISPATCH_LOOSE_PRODUCT = "dis_loose_product";
    public static final String AWAIL_DISPATCH_LOOSE_SIZE = "dis_loose_size";
    public static final String AWAIL_DISPATCH_LOOSE_QUANTITY = "dis_loose_quantity";
    public static final String AWAIL_DISPATCH_LOOSE_WEIGHT = "dis_loose_weight";
    public static final String AWAIL_DISPATCH_LOOSE_THICKNESS = "dis_loose_thickness";
    public static final String AWAIL_DISPATCH_BUNDLE_BARCODE = "dis_bundle_barcode";
    public static final String AWAIL_DISPATCH_BUNDLE_COUNT = "dis_bundle_count";
    public static final String AWAIL_DISPATCH_BUNDLE_WEIGHT = "dis_bundle_weight";
    public static final String AWAIL_RECEIVE_ATTACHMENTS = "cr_attachments";
    public static final String AWAIL_RECEIVE_VESSEL_NAME = "cr_vessel_name";
    public static final String AWAIL_RECEIVE_GRN = "cr_grn";
    public static final String AWAIL_RECEIVE_COIL_NO = "cr_coil_no";
    public static final String AWAIL_RECEIVE_COIL_BARCODE = "cr_coil_barcode";
    public static final String AWAIL_RECEIVE_COIL_WIDTH = "cr_coil_width";
    public static final String AWAIL_RECEIVE_COIL_THICKNESS = "    cr_coil_thickness";
    public static final String AWAIL_RECEIVE_COIL_LENGTH = "cr_coil_length";
    public static final String AWAIL_RECEIVE_COIL_NET_WEIGHT = "cr_coil_net_weight";
    public static final String AWAIL_RECEIVE_COIL_GROSS_WEIGHT = "cr_coil_gross_weight";
    public static final String AWAIL_RECEIVE_COIL_REMARK = "cr_coil_remark";
    public static final String AWAIL_RECEIVE_COIL_IMAGE = "cr_coil_image";
    public static final String AWAIL_QA_BARCODE = "qa_barcode";
    public static final String AWAIL_QA_ATTACHMENT = "qa_attachment";
    public static final String AWAIL_QA_SIZE = "qa_size";
    public static final String AWAIL_QA_LENGTH = "qa_length";
    public static final String AWAIL_QA_WEIGHT = "qa_weight";
    public static final String AWAIL_QA_COUNT = "qa_count";
    public static final String AWAIL_QA_REMARKS = "qa_remarks";
    public static final String AWAIL_QA_IMAGE = "qa_image";
    public static final String AWAIL_INFO_BARCODE = "inf_barcode";
    public static final String AWAIL_INFO_PARTIAL_COIL_ATTACHMENTS = "inf_partial_coil_attachments";
    public static final String AWAIL_INFO_PARTIAL_SLIT_ATTACHMENTS = "inf_partial_slit_attachments";
    public static final String AWAIL_INFO_SCRAP_COIL_ATTACHMENTS = "inf_scrap_coil_attachments";
    public static final String AWAIL_INFO_SCRAP_SLIT_ATTACHMENTS = "inf_scrap_slit_attachments";
    public static final String AWAIL_INFO_IMAGE = "inf_image";
    public static final String AWAIL_INFO_USED_WEIGHT = "inf_used_weight";
    public static final String AWAIL_INFO_REMAINING_WEIGHT = "inf_remaining_weight";
    public static final String AWAIL_INFO_PARTIAL_USE_REMARK = "inf_partial_use_remark";
    public static final String AWAIL_INFO_PARTIAL_USE_IMAGE = "inf_partial_use_image";
    public static final String AWAIL_INFO_SCRAP_REMARK = "inf_scrap_remark";
    public static final String AWAIL_INFO_SCRAP_IMAGE = "inf_scrap_image";
    public static final String AWAIL_PRODUCTION_PRODUCT_LENGTH = "prd_product_length";
    public static final String AWAIL_PRODUCTION_SCRAP_WEIGHT = "prd_scrap_weight";
    public static final String AWAIL_PRODUCTION_COIL_WEIGHT = "prd_coil_weight";
    public static final String AWAIL_PRODUCTION_SLIT_WEIGHT = "prd_slit_weight";
    public static final String AWAIL_PRODUCTION_BUNDLE_USED_COUNT = "prd_bundle_used_count";
    public static final String AWAIL_PRODUCTION_BUNDLE_REMARKS = "prd_bundle_remarks";
    public static final String AWAIL_PRODUCTION_SLIT_REMARK = "prd_slit_remarks";
    public static final String AWAIL_PRODUCTION_COIL_REMARK = "prd_coil_remarks";
    public static final String AWAIL_PRODUCTION_SCRAP_REMARK = "prd_scrap_remarks";
    public static final String AWAIL_PRODUCTION_BARCODE = "prd_barcode";
    public static final String AWAIL_PRODUCTION_SLIT_COIL_CODE = "prd_coil_code";
    public static final String AWAIL_PRODUCTION_SLIT_COIL_TYPE = "prd_coil_type";
    public static final String AWAIL_PRODUCTION_SLIT_WIDTH = "prd_slit_width";
    public static final String AWAIL_PRODUCTION_SLIT_LENGTH = "prd_slit_length";
    public static final String AWAIL_PRODUCTION_SLIT_THICKNESS = "prd_slit_thickness";
    public static final String AWAIL_PRODUCTION_BUNDLE_ATTACHMENTS = "prd_bundle_attachments";
    public static final String AWAIL_PRODUCTION_COIL_ATTACHMENTS = "prd_coil_attachments";
    public static final String AWAIL_PRODUCTION_SLIT_ATTACHMENTS = "prd_slit_attachments";
    public static final String AWAIL_BUNDLING_COUNT = "tag_bundle_count";
    public static final String AWAIL_BUNDLING_LENGTH = "tag_bundle_length";
    public static final String AWAIL_BUNDLING_THICKNESS = "tag_bundle_thickness";
    public static final String AWAIL_BUNDLING_WEIGHT = "tag_bundle_weight";
    public static final String AWAIL_BUNDLING_TIME_LIMIT = "tag_bundle_time_limit";
    public static final String AWAIL_CTL_COIL_WEIGHT = "ctl_coil_weight";
    public static final String AWAIL_CTL_SLIT_WEIGHT = "ctl_slit_weight";
    public static final String AWAIL_CTL_BUNDLE_USED_COUNT = "ctl_bundle_used_count";
    public static final String AWAIL_CTL_BUNDLE_REMARKS = "ctl_bundle_remarks";
    public static final String AWAIL_CTL_SLIT_REMARK = "ctl_slit_remarks";
    public static final String AWAIL_CTL_COIL_REMARK = "ctl_coil_remarks";
    public static final String AWAIL_CTL_SCRAP_REMARK = "ctl_scrap_remarks";
    public static final String AWAIL_CTL_BARCODE = "ctl_barcode";
    public static final String AWAIL_CTL_BUNDLING_COUNT = "ctl_bundle_count";
    public static final String AWAIL_CTL_BUNDLING_LENGTH = "ctl_bundle_length";
    public static final String AWAIL_CTL_BUNDLING_THICKNESS = "ctl_bundle_thickness";
    public static final String AWAIL_CTL_BUNDLING_WEIGHT = "ctl_bundle_weight";
}