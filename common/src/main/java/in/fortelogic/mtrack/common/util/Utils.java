package in.fortelogic.mtrack.common.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.preference.PreferenceManager;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Sreejith VS on 1/2/18.
 */

public class Utils {
    public static boolean isValidEmailAddress(String emailId) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(emailId).matches();

    }

    public static void setDefaultFont(View view, Context context) {
        if (view != null) {
            if (view instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) view;
                for (int i = 0; i < viewGroup.getChildCount(); i++) {
                    View child = viewGroup.getChildAt(i);
                    if (child != null) {
                        setDefaultFont(child, context);
                    }
                }
            } else if (view instanceof TextView) {
                TextView textView = (TextView) view;
                if (textView != null) {
                    textView.setTypeface(getEncodeSansMedium(context));
                }
            }
        }
    }

    private static Typeface getEncodeSansMedium(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/EncodeSans-Medium.ttf");
    }

    public static Typeface getEncodeSansBold(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/EncodeSans-Bold.ttf");
    }

    public static Typeface getEncodeSansRegular(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/EncodeSans-Regular.ttf");
    }

    public static Typeface getEncodeSansSemiBold(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/EncodeSans-SemiBold.ttf");
    }

    public static boolean isConnectedToInternet(Context context) {
        boolean hasInternet = false;

        if (context != null) {
            ConnectivityManager cm =
                    (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

            if (cm != null) {
                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                hasInternet = activeNetwork != null &&
                        activeNetwork.isConnectedOrConnecting();
            }
        }

        return hasInternet;
    }

    public static Spanned getSpannedText(String text) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT);
        } else {
            return Html.fromHtml(text);
        }
    }

    public static void writeToSharedPreference(Context context, String prefKey, Object value) {
        if (context != null && prefKey != null) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(prefKey, new Gson().toJson(value));
            editor.apply();
        }
    }

    public static void writeToSharedPreference(Context context, String prefKey, String value) {
        if (context != null && prefKey != null) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(prefKey, value);
            editor.apply();
        }
    }

    public static String readFromSharedPreference(Context context, String prefKey) {
        if (context != null && prefKey != null) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
            return preferences.getString(prefKey, null);
        }
        return null;
    }

    public static String formatDecimal(float value) {
        NumberFormat format = new DecimalFormat("##,##,###.##");
        return format.format(value);
    }

    public static String formatTime(long value){
        DateFormat timeInstance = new SimpleDateFormat("hh:mm");
        timeInstance.format(Calendar.getInstance().getTime());
        return timeInstance.format(value);
    }

    public static String formatDecimal(String number) {
        double amount = Double.parseDouble(number);
        DecimalFormat formatter = new DecimalFormat("##,##,###.##");
        Log.e("FORMATTER", formatter.format(amount));
        return formatter.format(amount);
    }

    public static boolean isValidString(String value) {
        return !TextUtils.isEmpty(value.trim()) && !value.trim().equals(".");
    }

}
