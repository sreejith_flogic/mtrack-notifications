package in.fortelogic.mtrack.common.validation;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;

import java.util.HashMap;

import in.fortelogic.mtrack.common.base.database.BaseSQLiteHelper;
import in.fortelogic.mtrack.common.base.database.FieldType;

public class ValidationTable extends BaseSQLiteHelper<Validation> {
    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "validation_db";
    private static final String TABLE_NAME = "validation_table";

    private static final String FIELD_KEY = "key";
    private static final String FIELD_MIN = "min";
    private static final String FIELD_MAX = "max";
    private static final String FIELD_MIN_LENGTH = "min_length";
    private static final String FIELD_MAX_LENGTH = "max_length";
    private static final String FIELD_TYPE = "type";
    private static final String FIELD_REGX = "regx";
    private static final String FIELD_DEFAULT_VALUE = "default_value";
    private static final String FIELD_REQUIRED = "required";


    private static ValidationTable instance = null;

    private ValidationTable(Context context) {
        super(context, DB_NAME, DB_VERSION);
    }

    public static ValidationTable getInstance(Context context) {
        if (instance == null) {
            instance = new ValidationTable(context);
        }

        return instance;
    }

    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Override
    protected Validation loadObject(Cursor cursor) {
        Validation validation = new Validation();
        if (cursor != null) {
            validation.setId(cursor.getLong(cursor.getColumnIndex(FIELD_ID)));
            validation.setKey(cursor.getString(cursor.getColumnIndex(FIELD_KEY)));
            validation.setMin(cursor.getDouble(cursor.getColumnIndex(FIELD_MIN)));
            validation.setMax(cursor.getDouble(cursor.getColumnIndex(FIELD_MAX)));
            validation.setMinLength(cursor.getInt(cursor.getColumnIndex(FIELD_MIN_LENGTH)));
            validation.setMaxLength(cursor.getInt(cursor.getColumnIndex(FIELD_MAX_LENGTH)));
            validation.setType(cursor.getString(cursor.getColumnIndex(FIELD_TYPE)));
            validation.setRegx(cursor.getString(cursor.getColumnIndex(FIELD_REGX)));
            validation.setRequired(cursor.getInt(cursor.getColumnIndex(FIELD_REQUIRED)) == 1);
            validation.setDefaultValue(cursor.getString(cursor.getColumnIndex(FIELD_DEFAULT_VALUE)));
        }

        return validation;
    }

    @Override
    protected ContentValues getContentValues(Validation validation) {
        ContentValues values = new ContentValues();
        if (validation != null) {
            values.put(FIELD_KEY, validation.getKey());
            values.put(FIELD_MIN, validation.getMin());
            values.put(FIELD_MAX, validation.getMax());
            values.put(FIELD_MIN_LENGTH, validation.getMinLength());
            values.put(FIELD_MAX_LENGTH, validation.getMaxLength());
            values.put(FIELD_TYPE, validation.getType());
            values.put(FIELD_REGX, validation.getRegx());
            values.put(FIELD_REQUIRED, validation.isRequired());
            values.put(FIELD_DEFAULT_VALUE, validation.getDefaultValue());
        }

        return values;
    }

    @Override
    protected HashMap<String, FieldType> getFields() {
        HashMap<String, FieldType> map = new HashMap<>();
        map.put(FIELD_KEY, FieldType.Text);
        map.put(FIELD_MIN, FieldType.Text);
        map.put(FIELD_MAX, FieldType.Text);
        map.put(FIELD_MIN_LENGTH, FieldType.Text);
        map.put(FIELD_MAX_LENGTH, FieldType.Text);
        map.put(FIELD_TYPE, FieldType.Text);
        map.put(FIELD_REGX, FieldType.Text);
        map.put(FIELD_REQUIRED, FieldType.Long);
        map.put(FIELD_DEFAULT_VALUE, FieldType.Text);
        return map;
    }

    @Override
    protected Long getId(Validation validation) {
        return validation.getId();
    }

    public Validation getValidation(String key) {
        synchronized (lock) {
            Validation validation = TextUtils.isEmpty(key) ? null : findByField(FIELD_KEY, key);
            return validation != null && validation.isRequired() ? validation : null;
        }
    }

    public void insertOrUpdate(Validation validation) {
        synchronized (lock) {
            if (validation != null) {
                // check if it has an id.
                if (validation.getId() != 0) {
                    update(validation);
                } else {
                    // check for an entry with same key
                    Validation saved = getValidation(validation.getKey());
                    if (saved != null) {
                        updateByFields(validation,
                                new String[]{FIELD_KEY}, new String[]{validation.getKey()});
                    } else {
                        insert(validation);
                    }
                }
            }
        }
    }
}
