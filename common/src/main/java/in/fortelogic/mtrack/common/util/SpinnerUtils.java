package in.fortelogic.mtrack.common.util;

import android.widget.Spinner;

import in.fortelogic.mtrack.common.base.BaseSpinnerAdapter;
import in.fortelogic.mtrack.common.base.BaseSpinnerModel;

public class SpinnerUtils {
    public static Spinner initSpinner(Spinner spinner, BaseSpinnerModel[] items) {
        return initSpinner(spinner, items, null);
    }

    public static Spinner initSpinner(Spinner spinner, BaseSpinnerModel[] items, BaseSpinnerModel selected) {
        if (spinner != null) {
            BaseSpinnerAdapter adapter = new BaseSpinnerAdapter(spinner.getContext(), items);
            spinner.setAdapter(adapter);

            if (selected != null) {
                spinner.setSelection(adapter.getIndex(selected),false);
            }
        }

        return spinner;
    }

    public static BaseSpinnerModel getSelectedItem(Spinner spinner) {
        BaseSpinnerModel model = null;

        if (spinner != null) {
            model = (BaseSpinnerModel) spinner.getSelectedItem();
        }

        return model;
    }
}
