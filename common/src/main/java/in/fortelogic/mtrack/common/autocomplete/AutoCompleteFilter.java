package in.fortelogic.mtrack.common.autocomplete;

import android.content.Context;
import android.widget.Filter;

import java.util.ArrayList;

import in.fortelogic.mtrack.common.base.BaseAutoCompleteModel;

public abstract class AutoCompleteFilter<T extends BaseAutoCompleteModel> extends Filter {
    private final Context mContext;
    private final Callback mCallback;

    AutoCompleteFilter(Context context, Callback callback) {
        mContext = context;
        mCallback = callback;
    }

    @Override
    public CharSequence convertResultToString(Object resultValue) {
        if (resultValue instanceof BaseAutoCompleteModel) {
            ((BaseAutoCompleteModel) resultValue).getName();
        }

        return super.convertResultToString(resultValue);
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        if (constraint != null && mCallback != null) {
            ArrayList<T> suggestions = getSearchResults(mContext, constraint.toString());

            if (suggestions != null) {
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();

                return filterResults;
            }
        }

        return new FilterResults();
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        if (results != null && mCallback != null) {
            ArrayList<BaseAutoCompleteModel> filterList =
                    (ArrayList<BaseAutoCompleteModel>) results.values;
            mCallback.onPublish(filterList);
        }
    }

    public abstract ArrayList<T> getSearchResults(Context context, String searchKey);

    public interface Callback<T extends BaseAutoCompleteModel> {
        void onPublish(ArrayList<T> values);
    }
}
