package in.fortelogic.mtrack.common.constants;

import static in.fortelogic.mtrack.common.BuildConfig.BASE_URL;

/**
 * @author Rahul Raveendran V P
 * Created on 07/04/18 @ 12:35 PM
 * https://github.com/rahulrvp
 */

public class WebserviceURLs {
    public static final String LOGIN_URL = BASE_URL + "/users/login.json";
    public static final String PULL_PRODUCTS_URL = BASE_URL + "/product_grades.json";
    public static final String PULL_GRADES_URL = BASE_URL + "/grades.json";
    public static final String PULL_SIZE_URL = BASE_URL + "/billet_sizes.json";
    public static final String PULL_BUNDLE_SIZE_URL = BASE_URL + "/sizes.json";
    public static final String CCM_STRANDS_URL = BASE_URL + "/ccm_strands.json";
    public static final String ROLLING_LINES_URL = BASE_URL + "/rolling_lines.json";
    public static final String PULL_TYPES_URL = BASE_URL + "/product_types.json";
    public static final String PULL_CATEGORIES_URL = BASE_URL + "/product_categories.json";
    public static final String PULL_PRODUCT_SIZE_URL = BASE_URL + "/product_sizes.json";
    public static final String PULL_REASONS_URL = BASE_URL + "/reasons.json";

    public static final String ADD_ROLLED_PRODUCTS_URL = BASE_URL + "/rollings/enqueue_billet.json";
    public static final String RECORD_COBBLE_URL = BASE_URL + "/rollings/add_cobble.json";
    public static final String ADD_TAGGED_ITEM_URL = BASE_URL + "/product_bundles";

    public static final String SEARCH_PURCHASE_ORDER = BASE_URL + "/purchase_orders/find_po.json";
    public static final String PULL_STACKS_URL = BASE_URL + "/stack_locations.json";
    public static final String PULL_COIL_TYPE_URL = BASE_URL + "/coil_types.json";
    public static final String ADD_COIL_URL = BASE_URL + "/coils/receive_coil.json";
    public static final String ADD_BUNDLE_URL = BASE_URL + "/produces.json";
    public static final String PULL_PRODUCT_LINES_URL = BASE_URL + "/production_lines.json";
    public static final String SUBMIT_SLITS_URL = BASE_URL + "/coil_slits.json";
    public static final String UPDATE_SLITS_URL = BASE_URL + "/coil_slits/update_slit.json";
    public static final String PULL_PURCHASE_ORDER = BASE_URL + "/purchase_orders/pending.json";
    public static final String PULL_PACKING_ITEMS = BASE_URL + "/packing_items/pending.json";
    public static final String SEARCH_PACKING_ITEM = BASE_URL + "/packing_items/find_item.json";
    public static final String START_SLITTING_URL = BASE_URL + "/coils/start_slitting.json";
    public static final String ADD_SLIT_TO_PRODUCTION = BASE_URL + "/coil_slits/add_to_production.json";
    public static final String ADD_LOOSE_SLIT_TO_PRODUCTION = BASE_URL + "/coil_slits/add_loose_slits.json";
    public static final String ADD_BUNDLE_TO_PRODUCTION = BASE_URL + "/produces/add_to_production.json";
    public static final String DISPATCHES = BASE_URL + "/dispatches.json";
    public static final String FETCH_BARCODE_DETAILS_URL = BASE_URL + "/get_tag_info.json";
    public static final String FETCH_PRODUCTION_BARCODE_DETAILS_URL = BASE_URL + "/production_sessions/get_tag_info.json";
    public static final String COIL_SCRAP_URL = BASE_URL + "/coils/scrap.json";
    public static final String SLIT_SCRAP_URL = BASE_URL + "/coil_slits/scrap.json";
    public static final String COIL_PARTIAL_USE_API = BASE_URL + "/coils/partial_use.json";
    public static final String SLIT_PARTIAL_USE_API = BASE_URL + "/coil_slits/partial_use.json";
    public static final String FETCH_BUNDLE_URL = BASE_URL + "/produces/find_bundle.json";
    public static final String FETCH_SLIT_MACHINES_URL = BASE_URL + "/slit_machines.json";
    public static final String RE_BUNDLE_API = BASE_URL + "/produces/rebundle.json";
    public static final String FETCH_COIL_DETAILS_URL = BASE_URL + "/coils/find_coil.json";
    public static final String SUBMIT_INSPECTION = BASE_URL + "/produce_inspections.json";
    public static final String FETCH_CONDITIONS_URL = BASE_URL + "/conditions.json";
    public static final String FETCH_SLIT_DETAILS_URL = BASE_URL + "/coil_slits/find_slit.json";
    public static final String SET_PRODUCTION_API = BASE_URL + "/production_sessions.json";
    public static final String UPDATE_SCRAP_URL = BASE_URL + "/production_sessions/add_scrap.json";
    public static final String FETCH_SALE_ORDER_URL = BASE_URL + "/sale_orders/find_so.json";
    public static final String TEMPLATES_URL = BASE_URL + "/slit_plans.json";
    public static final String GET_COIL_POSITION_URL = BASE_URL + "/positions.json";
    public static final String ADD_COIL_MEASUREMENT_URL = BASE_URL + "/measurements.json";
    public static final String GET_VALIDATIONS_URL = BASE_URL + "/validations.json";
    public static final String FETCH_BUNDLE_OFFLINE_URL = BASE_URL + "/produces/in_stock.json";

    public static String START_SLITTING_URL(long id) {
        return BASE_URL + "/coils/" + id + "/start_slitting.json";
    }
}