package in.fortelogic.mtrack.common.base;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.github.rahulrvp.android_utils.BaseFont;
import com.github.rahulrvp.android_utils.TextViewUtils;
import com.google.android.material.snackbar.Snackbar;

import in.fortelogic.mtrack.common.R;
import in.fortelogic.mtrack.common.login.User;
import in.fortelogic.mtrack.common.ui.ESBold;
import in.fortelogic.mtrack.common.ui.ESMedium;
import in.fortelogic.mtrack.common.ui.ESSemiBold;
import in.fortelogic.mtrack.common.validation.ValidationUtils;

public class MTrackActivity extends BaseActivity {

    private ProgressBar mSyncProgress;
    private TextView mSyncItemCountText;
    private ImageView mSyncIcon;
    private ProgressDialog mProgressDialog;
    private AlertDialog mOverFlowMenu;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*
         * Activity orientation change
         * -------------------------
         * If developer wants to lock all the activities in portrait mode, instead of
         * doing it individually for each activity in manifest, they can do it like this.
         *
         * Uncomment the following line.
         */

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);

        // apply common font
        BaseFont baseFont = getActivityFont();
        if (baseFont != null) {
            baseFont.apply(getRootView());
        }

        // setup header
        setupHeader();

        // hideSyncButton();
    }

    /**
     * This method is responsible for providing the font object.
     * Override this to return your own font object.
     *
     * @return BaseFont instance.
     */
    public BaseFont getActivityFont() {
        return ESMedium.getInstance();
    }

    private void setupHeader() {
        User currentUser = User.getInstance(mContext);
        if (currentUser.isLoggedIn()) {
            TextViewUtils.setText(
                    (TextView) findViewById(R.id.user_name), currentUser.getName());
        }

        mSyncIcon = findViewById(R.id.sync_icon_img);
        mSyncProgress = findViewById(R.id.sync_progress);
        mSyncItemCountText = findViewById(R.id.sync_remaining_count_text);
    }

    protected void setHeaderText(int res) {
        setHeaderText(getString(res));
    }

    protected void setHeaderText(String title) {
        TextViewUtils.setText((TextView) findViewById(R.id.header_text), title);
    }

    public void onLogoutClicked(View view) {
        logoutUser();
        dismisSMenu();

        PackageManager packageManager = getPackageManager();
        Intent launcherIntent = packageManager.getLaunchIntentForPackage(getPackageName());

        if (launcherIntent != null) {
            launcherIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            launcherIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(launcherIntent);
        }
    }

    public void onRefreshClicked(View view) {
        dismisSMenu();
    }

    public void onOverflowMenuClicked(View view) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.overflow_menu_layout, null);

        if (dialogView != null) {
            ESMedium.getInstance().apply(dialogView);
            builder.setView(dialogView);

            TextView versionName = dialogView.findViewById(R.id.app_version_text);

            try {
                PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                String version = pInfo.versionName;
                TextViewUtils.setText(versionName, R.string.version, version);
            } catch (PackageManager.NameNotFoundException ignored) {
            }

            mOverFlowMenu = builder.create();
            mOverFlowMenu.requestWindowFeature(Window.FEATURE_NO_TITLE);
            WindowManager.LayoutParams layoutParams = mOverFlowMenu.getWindow().getAttributes();

            layoutParams.gravity = Gravity.TOP | Gravity.RIGHT;
            layoutParams.x = 20;   //x position
            layoutParams.y = 20;   //y position
            mOverFlowMenu.show();
            mOverFlowMenu.getWindow().setLayout(600, layoutParams.height); //Controlling width and height.
        }

    }

    protected void dismisSMenu() {
        if (mOverFlowMenu != null && mOverFlowMenu.isShowing()) {
            mOverFlowMenu.dismiss();
        }
    }

    /**
     * This method is responsible for logout the user from device.
     * A default logout is implemented. The developer can override
     * this method and implement their own logout later.
     */
    public void logoutUser() {
        User.logout(mContext);
    }

    // sync helpers

    /**
     * Override this method to call sync operation
     *
     * @param view onClick method param
     */
    public void onSyncClicked(View view) {
        /*
         * IMPORTANT NOTICE:
         * Developer should call the below three methods as needed to get the sync work properly.
         *
         * - disableSyncUX()
         * - enableSyncUX()
         * - syncUpdateRemainingCount()
         */

        showSnackbar(getString(R.string.msg_sync_not_implemented), 20000);
    }

    public void disableSyncUX() {
        if (mSyncProgress != null) {
            mSyncProgress.setVisibility(View.VISIBLE);
        }

        if (mSyncIcon != null) {
            mSyncIcon.setVisibility(View.GONE);
        }
    }

    public void enableSyncUX() {
        if (mSyncProgress != null) {
            mSyncProgress.setVisibility(View.GONE);
        }

        if (mSyncIcon != null) {
            mSyncIcon.setVisibility(View.VISIBLE);
        }
    }

    public void syncUpdateRemainingCount(String count) {
        TextViewUtils.setText(mSyncItemCountText, count);
    }

    protected void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
        }

        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    protected void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing() && !isFinishing()) {
            try {
                mProgressDialog.dismiss();
            } catch (Exception ignored) {
            }
        }
    }

    protected void hideSyncButton() {
        RelativeLayout btnLayout = findViewById(R.id.common_header_sync_button);
        if (btnLayout != null) {
            btnLayout.setVisibility(View.GONE);
        }
    }

    protected void launchActivity(Class activity, Bundle bundle) {
        Intent intent = new Intent(mContext, activity);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
    }

    protected void showSuccessToast(String message) {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast_layout,
                (ViewGroup) findViewById(R.id.toast_layout_root));

        ESBold.getInstance().apply(layout);

        ImageView image = (ImageView) layout.findViewById(R.id.toast_image);
        image.setImageResource(R.drawable.tick_circle);
        TextView text = (TextView) layout.findViewById(R.id.toast_text);
        text.setText(message);

        Toast toast = new Toast(mContext);
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    private void createSnackbar(String message) {
        if (!TextUtils.isEmpty(message)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(in.fortelogic.mtrack.common.base.R.layout.snackbar_layout, null);

            if (dialogView != null) {
                builder.setView(dialogView);

                final AlertDialog alertDialog = builder.create();
                if (alertDialog != null) {
                    alertDialog.setCancelable(true);
                    alertDialog.setCanceledOnTouchOutside(true);

                    TextView messageTV = dialogView.findViewById(in.fortelogic.mtrack.common.base.R.id.message);
                    if (messageTV != null) {
                        messageTV.setText(message);
                        ESMedium.getInstance().apply(messageTV);
                    }

                    Button dismissButton = dialogView.findViewById(in.fortelogic.mtrack.common.base.R.id.dismiss_button);
                    if (dismissButton != null) {
                        ESSemiBold.getInstance().apply(dismissButton);
                        dismissButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                alertDialog.dismiss();
                            }
                        });
                    }

                    alertDialog.show();

                    Window window = alertDialog.getWindow();
                    window.setGravity(Gravity.BOTTOM);
                }
            }
        }
    }


    protected void showSnackbarShort(int resId) {
        showSnackbar(resId, Snackbar.LENGTH_SHORT);
    }

    protected void showSnackbarShort(String message) {
        createSnackbar(message);
    }

    protected void showSnackbar(int resId, int duration) {
        createSnackbar(getString(resId));
    }

    protected void showSnackbar(String message, int duration) {
        createSnackbar(message);
    }

    protected boolean shouldValidate(String key) {
        return ValidationUtils.shouldValidate(mContext, key);
    }

    protected void prepareFieldForValidation(EditText editText, String key) {
        ValidationUtils.prepareEditText(editText, key);
    }

    protected boolean validate(EditText editText, String key) {
        return ValidationUtils.validate(editText, key);
    }

    protected void showAppCloseConfirmationDialog() {
        new AlertDialog.Builder(mContext, R.style.dialogTheme)
                .setTitle(getString(R.string.exit_application))
                .setMessage(getString(R.string.sure_want_to_exit))
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                }).setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).show();
    }
}
