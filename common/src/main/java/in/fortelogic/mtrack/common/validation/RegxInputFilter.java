package in.fortelogic.mtrack.common.validation;

import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegxInputFilter implements InputFilter {
    private Pattern mPattern;

    public RegxInputFilter(String regx) {
        if (!TextUtils.isEmpty(regx)) {
            mPattern = Pattern.compile(regx);
        }
    }

    @Override
    public CharSequence filter(
            CharSequence source, int start, int end, Spanned destination, int dStart, int dEnd) {

        if (source.equals("")) return null;

        StringBuilder builder = new StringBuilder(destination);
        builder.replace(dStart, dEnd, source.subSequence(start, end).toString());
        Matcher matcher = mPattern.matcher(builder);
        if (!matcher.matches())
            return "";

        return null;
    }
}
