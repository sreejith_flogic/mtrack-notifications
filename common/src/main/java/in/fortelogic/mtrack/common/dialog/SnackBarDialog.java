package in.fortelogic.mtrack.common.dialog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import com.github.rahulrvp.android_utils.TextViewUtils;

import in.fortelogic.mtrack.common.R;
import in.fortelogic.mtrack.common.ui.ESMedium;
import in.fortelogic.mtrack.common.ui.ESSemiBold;

public class SnackBarDialog extends AlertDialog {
    private String mMessage;

    public SnackBarDialog(@NonNull Context context, String message) {
        super(context);

        mMessage = message;
        prepareDialog();
    }

    public SnackBarDialog(@NonNull Context context, int message) {
        super(context);

        mMessage = context.getString(message);
        prepareDialog();
    }

    private void prepareDialog() {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        @SuppressLint("InflateParams") View dialogView =
                inflater.inflate(R.layout.snackbar_layout, null, false);

        if (dialogView != null) {
            setView(dialogView);

            setCancelable(false);
            setCanceledOnTouchOutside(false);

            TextView messageTV = dialogView.findViewById(R.id.message);
            TextViewUtils.setText(messageTV, mMessage);
            ESMedium.getInstance().apply(messageTV);

            Button dismissButton = dialogView.findViewById(R.id.dismiss_button);
            ESSemiBold.getInstance().apply(dismissButton);
            dismissButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dismiss();
                }
            });
        }
    }

    @Override
    public void show() {
        super.show();

        Window window = getWindow();
        if (window != null) {
            window.setGravity(Gravity.BOTTOM);
        }
    }
}
