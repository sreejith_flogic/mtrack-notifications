package in.fortelogic.mtrack.common.constants;

public interface InterfaceNames {
    String COIL_RECEIVE_APP = "receive";
    String SLIT_APP = "slitting";
    String DISPATCH_APP = "slitting";
    String PRODUCTION_APP = "slitting";
    String TAGGING_APP = "slitting";
    String COIL_MEASUREMENT_INTERFACE = "measurement";
    String INFO_APP = "slitting";
    String QA_APP = "receive";
    String RE_BUNDLING_APP = "receive";
}
