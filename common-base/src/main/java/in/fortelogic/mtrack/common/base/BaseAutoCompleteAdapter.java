package in.fortelogic.mtrack.common.base;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

public class BaseAutoCompleteAdapter extends ArrayAdapter<BaseAutoCompleteModel> {
    private Context mContext;
    private List<BaseAutoCompleteModel> mItems = new ArrayList<>();
    private Filter mFilter;

    public BaseAutoCompleteAdapter(@NonNull Context context, Filter filter) {
        super(context, R.layout.layout_auto_complete_view_item);
        this.mContext = context;
        mFilter = filter;

    }

    public BaseAutoCompleteAdapter(@NonNull Context context) {
        super(context, R.layout.layout_auto_complete_view_item);
        this.mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null) {
            view = LayoutInflater.from(mContext).
                    inflate(R.layout.layout_auto_complete_view_item, parent, false);
        }

        BaseAutoCompleteModel item = mItems.get(position);

        if (item != null && view instanceof TextView) {
            ((TextView) view).setText(item.getName());
        }

        return view;
    }

    @Override
    public BaseAutoCompleteModel getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return mFilter;
    }

    public BaseAutoCompleteAdapter setFilter(Filter filter) {
        mFilter = filter;
        return this;
    }

    public void setValues(ArrayList<BaseAutoCompleteModel> items) {
        mItems.clear();
        if (items != null) {
            mItems.addAll(items);
        }
        notifyDataSetChanged();
    }

}
