package in.fortelogic.mtrack.common.base;

public interface BaseSpinnerModel {

    long getId();

    String getName();

}
