package in.fortelogic.mtrack.common.base;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class BaseCacheModel extends BaseModel {

    // TODO: 17/05/18 think about adding a method to take file name
    private static final String PREF_FILE = "mTrack";

    private static SharedPreferences getSharedPreferences(Context context) {
        SharedPreferences sp = null;

        if (context != null) {
            sp = context.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE);
        }

        return sp;
    }

    protected static void save(Context context, Object[] objects, String keyName) {
        if (objects != null) {
            SharedPreferences sp = getSharedPreferences(context);
            if (sp != null) {
                String json = new Gson().toJson(objects);
                sp
                        .edit()
                        .putString(keyName, json)
                        .apply();
            }
        }
    }

    protected static <T> void save(Context context, List<T> objects, String keyName) {
        if (objects != null) {
            SharedPreferences sp = getSharedPreferences(context);
            if (sp != null) {
                String json = new Gson().toJson(objects);
                sp
                        .edit()
                        .putString(keyName, json)
                        .apply();
            }
        }
    }

    protected static String getSavedJson(Context context, String keyName) {
        String json = null;

        SharedPreferences sp = getSharedPreferences(context);
        if (sp != null) {
            json = sp.getString(keyName, null);
        }

        return json;
    }

    protected static <T> List<T> getSavedList(Context context, String keyName) {
        List<T> list = new ArrayList<>();

        String listJson = getSavedJson(context, keyName);
        if (!TextUtils.isEmpty(listJson)) {
            try {
                Type type = new TypeToken<List<T>>() {
                }.getType();
                list = new Gson().fromJson(listJson, type);
            } catch (Exception ignore) {
            }
        }

        return list;
    }

    protected void save(Context context, String keyName) {
        SharedPreferences sp = getSharedPreferences(context);
        if (sp != null) {
            String json = new Gson().toJson(this);
            if (!TextUtils.isEmpty(json)) {
                sp
                        .edit()
                        .putString(keyName, json)
                        .apply();
            }
        }
    }

    protected static void removeFromCache(Context context, @NotNull String[] keys) {
        SharedPreferences sp = getSharedPreferences(context);
        if (sp != null) {
            SharedPreferences.Editor editor = sp.edit();

            for (String key : keys) {
                editor.remove(key);
            }

            editor.apply();
        }
    }

    public static void removeFromCache(Context context, String key) {
        SharedPreferences sp = getSharedPreferences(context);
        if (sp != null) {
            SharedPreferences.Editor editor = sp.edit();

            editor.remove(key);

            editor.apply();
        }
    }

    protected static void emptyCache(Context context) {
        SharedPreferences sp = getSharedPreferences(context);
        if (sp != null) {
            SharedPreferences.Editor editor = sp.edit();
            editor.clear();
            editor.apply();
        }
    }
}
