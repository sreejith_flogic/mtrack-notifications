package in.fortelogic.mtrack.common.base.database;

public enum FieldType {
    String,
    Text,
    Autoincrement,
    Double,
    Long;


    @Override
    public java.lang.String toString() {
        switch (this) {
            case String:
                return "STRING";
            case Text:
                return "TEXT";
            case Double:
                return "DOUBLE";
            case Long:
                return "INTEGER";
            case Autoincrement:
                return "INTEGER PRIMARY KEY AUTOINCREMENT";
            default:
                return "";
        }
    }
}
