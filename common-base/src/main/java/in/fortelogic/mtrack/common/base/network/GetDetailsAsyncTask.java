package in.fortelogic.mtrack.common.base.network;

import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import java.util.HashMap;

import ch.bullfin.httpmanager.HTTPManager;
import ch.bullfin.httpmanager.Response;

public class GetDetailsAsyncTask<T extends GetDetailsBaseModel> extends AsyncTask<T, Void, T> {
    private static final String LOG_TAG = "GetDetailsAsyncTask";
    private Callback mCallback;

    public GetDetailsAsyncTask<T> setCallback(Callback callback) {
        mCallback = callback;
        return this;
    }

    @Override
    protected void onPreExecute() {
        if (mCallback != null) {
            mCallback.onTaskStart();
        }
    }

    @SafeVarargs
    @Override
    protected final T doInBackground(T... args) {
        if (args != null && args.length > 0) {
            String apiURL = args[0].getObjectURL();
            if (!TextUtils.isEmpty(apiURL)) {
                HTTPManager httpManager = new HTTPManager(apiURL);
                Response response;

                //noinspection unchecked
                HashMap<String, Object> params = args[0].getObjectArguments();
                if (params == null || params.size() == 0) {
                    Log.i(LOG_TAG, "No arguments provided!");
                    response = httpManager.get();
                } else {
                    response = httpManager.get(params);
                }

                if (response != null) {
                    if (response.getStatusCode() == 200) {
                        String responseBody = response.getResponseBody();
                        if (!TextUtils.isEmpty(responseBody)) {
                            try {
                                //noinspection unchecked
                                return (T) args[0].getObjectDetails(responseBody);
                            } catch (Exception e) {
                                Log.e(LOG_TAG, e.getMessage());
                            }
                        } else {
                            Log.e(LOG_TAG, "Empty response body received from " + apiURL);
                        }
                    } else {
                        Log.e(LOG_TAG, "API error. Code: " + response.getStatusCode());
                    }
                } else {
                    Log.e(LOG_TAG, "No response received from " + apiURL);
                }
            } else {
                Log.e(LOG_TAG, "No API URL found inside the provided object");
            }
        } else {
            Log.e(LOG_TAG, "No valid model provided with URL and params.");
        }

        return null;
    }

    @Override
    protected void onPostExecute(T results) {
        if (mCallback != null) {
            //noinspection unchecked
            mCallback.onTaskComplete(results);
        }
    }

    public interface Callback<T> {
        void onTaskStart();

        void onTaskComplete(T result);
    }
}
