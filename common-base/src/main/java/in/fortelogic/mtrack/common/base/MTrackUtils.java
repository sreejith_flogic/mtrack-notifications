package in.fortelogic.mtrack.common.base;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import android.util.TypedValue;

import androidx.annotation.NonNull;

import static android.content.Context.MODE_PRIVATE;

public class MTrackUtils {
    private static final String INTERFACE_NAME_FILE = "interface_file";
    private static final String INTERFACE_NAME_KEY = "interface_key";

    public static int dpToPx(float dp, @NonNull Context context) {
        return (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
    }

    public static String getAppVersion(Context context) {
        String appVersion = null;
        if (context != null && context.getPackageName() != null) {
            PackageManager pkgManager = context.getPackageManager();

            if (pkgManager != null) {
                PackageInfo pkgInfo = null;

                try {
                    pkgInfo = pkgManager.getPackageInfo(context.getPackageName(), 0);
                } catch (PackageManager.NameNotFoundException ignore) {
                }

                if (pkgInfo != null && pkgInfo.versionName != null) {
                    appVersion = pkgInfo.versionName + " (" + pkgInfo.versionCode + ")";
                }
            }
        }

        return appVersion;
    }

    public static int getAppVersionCode(Context context) {
        int appVersion = 0;
        if (context != null && context.getPackageName() != null) {
            PackageManager pkgManager = context.getPackageManager();

            if (pkgManager != null) {
                PackageInfo pkgInfo = null;

                try {
                    pkgInfo = pkgManager.getPackageInfo(context.getPackageName(), 0);
                } catch (PackageManager.NameNotFoundException ignore) {
                }

                if (pkgInfo != null) {
                    return pkgInfo.versionCode;
                }
            }
        }

        return appVersion;
    }

    public static String getAppName(Context context) {
        String appName = null;
        if (context != null && context.getPackageName() != null) {
            PackageManager pkgManager = context.getPackageManager();
            if (pkgManager != null) {
                ApplicationInfo appInfo = null;

                try {
                    appInfo = pkgManager.getApplicationInfo(context.getPackageName(), 0);
                } catch (PackageManager.NameNotFoundException ignored) {
                }

                if (appInfo != null) {
                    appName = (String) pkgManager.getApplicationLabel(appInfo);
                }
            }
        }

        return appName;
    }

    public static void saveInterfaceName(Context context, String interfaceName) {
        if (context != null) {
            SharedPreferences sp = context.getSharedPreferences(INTERFACE_NAME_FILE, MODE_PRIVATE);
            SharedPreferences.Editor editor = sp.edit();

            if (TextUtils.isEmpty(interfaceName)) {
                editor.remove(INTERFACE_NAME_KEY);
            } else {
                editor.putString(INTERFACE_NAME_KEY, interfaceName);
            }

            editor.apply();
        }
    }

    public static String getSavedInterfaceName(Context context) {
        if (context != null) {
            SharedPreferences sp = context.getSharedPreferences(INTERFACE_NAME_FILE, MODE_PRIVATE);
            return sp.getString(INTERFACE_NAME_KEY, null);
        }

        return null;
    }

    public static boolean isValidEmailAddress(String emailId) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(emailId).matches();

    }
}
