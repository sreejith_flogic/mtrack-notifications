package in.fortelogic.mtrack.common.base;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

public class BaseActivity extends AppCompatActivity {

    public Context mContext;
    private View mContentView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = this;

        /*
         * Activity orientation change
         * -------------------------
         * If developer wants to lock all the activities in portrait mode, instead of
         * doing it individually for each activity in manifest, they can do it like this.
         *
         * Uncomment the following line.
         */

        // setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);

        mContentView = findViewById(android.R.id.content);

        /*
         * Activity background color change
         * --------------------------------
         * Developer can set a common background color to all the activities in the app.
         *
         * Uncomment the following line and pass in a valid color resource id.
         */

        // setContentViewBackground(getActivityBackgroundColor());
    }

    public int getActivityBackgroundColor() {
        return ContextCompat.getColor(mContext, R.color.activityBackground);
    }

    private void setContentViewBackground(int color) {
        if (mContentView != null) {
            mContentView.setBackgroundColor(color);
        }
    }

    protected void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    protected View getRootView() {
        return mContentView;
    }


    protected void showExtConfirmationPopup() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.dialogTheme);
        builder.setMessage("Do you want to exit this page?");
        builder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.setNegativeButton("Cancel", null);
        builder.create().show();
    }
}
