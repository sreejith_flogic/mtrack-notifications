package in.fortelogic.mtrack.common.base.database;

import android.content.Context;

public abstract class BaseSQLiteCacheModel<Record> extends BaseSQLiteHelper<Record> {

    protected static final String FIELD_IS_SYNCED = "is_synced";

    private boolean isSynced;

    public BaseSQLiteCacheModel(Context context, String dbName, int dbVersion) {
        super(context, dbName, dbVersion);
    }

    public boolean isSynced() {
        return isSynced;
    }

    public void setSynced(boolean synced) {
        isSynced = synced;
    }
}
