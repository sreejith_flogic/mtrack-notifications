package in.fortelogic.mtrack.common.base.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public abstract class BaseSQLiteHelper<T> extends SQLiteOpenHelper {
    protected static final Boolean lock = true;
    private static final String LOG_TAG = BaseSQLiteHelper.class.getSimpleName();

    protected static final String FIELD_ID = "_id";

    public BaseSQLiteHelper(Context context, String dbName, int dbVersion) {
        super(context, dbName, null, dbVersion);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(generateCreateTableQuery());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE " + getTableName());
        onCreate(db);
    }

    /**
     * Generates SQL query to create a table with available tableName and field-Type HashMap.
     *
     * @return valid create table SQL query if valid arguments given, else null.
     */
    private String generateCreateTableQuery() {
        String query = null;

        String tableName = getTableName();
        HashMap<String, FieldType> fieldTypeMap = getFields();
        if (fieldTypeMap != null) {
            fieldTypeMap.put(FIELD_ID, FieldType.Autoincrement);
        }

        if (!TextUtils.isEmpty(tableName) && fieldTypeMap != null && fieldTypeMap.size() != 0) {
            StringBuilder createTableQuery = new StringBuilder("CREATE TABLE " + tableName + "(");

            Set<String> fieldNames = fieldTypeMap.keySet();
            boolean isFirstIteration = true;

            for (String fieldName : fieldNames) {
                if (isFirstIteration) {
                    isFirstIteration = false;
                } else {
                    createTableQuery.append(",");
                }

                String dataType = fieldTypeMap.get(fieldName).toString();
                createTableQuery.append(fieldName).append(" ").append(dataType);
            }

            query = createTableQuery + ")";
        }

        return query;
    }

    public void insert(T item) {
        synchronized (lock) {
            SQLiteDatabase db = getWritableDatabase();

            if (db != null) {
                db.insert(getTableName(), null, getContentValues(item));
                db.close();
            }
        }
    }

    public void update(T item) {
        synchronized (lock) {
            SQLiteDatabase db = getWritableDatabase();

            if (db != null && getId(item) != null) {
                db.update(getTableName(), getContentValues(item), FIELD_ID + "=" + getId(item), null);
                db.close();
            }
        }
    }

    public void updateByFields(T item, String[] fields, String[] values) {
        synchronized (lock) {
            SQLiteDatabase db = getWritableDatabase();

            if (db != null && fields != null && fields.length > 0
                    && values != null && fields.length == values.length) {

                StringBuilder builder = new StringBuilder();
                boolean isFirstIteration = true;
                for (String field : fields) {
                    if (isFirstIteration) {
                        isFirstIteration = false;
                    } else {
                        builder.append(" AND ");
                    }

                    builder.append(field).append("=?");
                }

                db.update(getTableName(), getContentValues(item), builder.toString(), values);
                db.close();
            }
        }
    }

    protected void delete(String key, String value) {
        synchronized (lock) {
            SQLiteDatabase db = getWritableDatabase();

            if (db != null) {
                db.delete(getTableName(), key + "=?", new String[]{value});
                db.close();
            }
        }
    }

    protected int delete(String key, ArrayList<String> values) {
        int count = 0;

        if (!TextUtils.isEmpty(key) && values != null && values.size() > 0) {
            String[] valuesArray = new String[values.size()];
            values.toArray(valuesArray);

            count = delete(key, valuesArray);
        }

        return count;
    }

    protected int delete(String fieldName, String[] values) {
        int count = 0;

        if (!TextUtils.isEmpty(fieldName) && values != null && values.length > 0) {
            String valuesCSV = TextUtils.join(",", values);
            if (!TextUtils.isEmpty(valuesCSV)) {
                synchronized (lock) {
                    SQLiteDatabase db = getWritableDatabase();
                    if (db != null) {
                        Log.d(LOG_TAG, "Deleting records with '" + fieldName + "' IN (" + valuesCSV + ")");

                        count = db.delete(getTableName(), fieldName + " IN (" + valuesCSV + ")", null);
                        db.close();
                    }
                }
            }
        }

        return count;
    }

    public void deleteAll() {
        synchronized (lock) {
            SQLiteDatabase db = getWritableDatabase();

            if (db != null) {
                db.delete(getTableName(), null, null);
                db.close();
            }
        }
    }

    /**
     * finder methods
     **/

    public ArrayList<T> findAll() {
        ArrayList<T> result = new ArrayList<>();

        synchronized (lock) {
            SQLiteDatabase db = getReadableDatabase();
            if (db != null) {
                Cursor cursor = db.query(getTableName(), null, null, null, null, null, null);
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        while (!cursor.isAfterLast()) {
                            result.add(loadObject(cursor));
                            cursor.moveToNext();
                        }
                    }

                    cursor.close();
                }

                db.close();
            }
        }

        return result;
    }

    public ArrayList<T> findWithLimit(int limit) {
        ArrayList<T> result = new ArrayList<>();

        synchronized (lock) {
            SQLiteDatabase db = getReadableDatabase();
            if (db != null) {
                Cursor cursor = db.rawQuery("SELECT * FROM " + getTableName() + " LIMIT " + limit, null);
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        while (!cursor.isAfterLast()) {
                            result.add(loadObject(cursor));
                            cursor.moveToNext();
                        }
                    }

                    cursor.close();
                }

                db.close();
            }
        }

        return result;
    }

    public T findByField(String fieldName, String value) {
        T object = null;

        synchronized (lock) {
            SQLiteDatabase db = getReadableDatabase();

            if (db != null) {
                Cursor cursor = db.query(getTableName(), null, fieldName + "='" + value + "'", null, null, null, null);
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        object = loadObject(cursor);
                    }

                    cursor.close();
                }

                db.close();
            }
        }

        return object;
    }

    public T findByField(String fieldName, long value) {
        T object = null;

        synchronized (lock) {
            SQLiteDatabase db = getReadableDatabase();

            if (db != null) {
                Cursor cursor = db.query(getTableName(), null, fieldName + "=" + value, null, null, null, null);
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        object = loadObject(cursor);
                    }

                    cursor.close();
                }

                db.close();
            }
        }

        return object;
    }

    public T findRandomByField(String fieldName, long value) {
        T object = null;

        synchronized (lock) {
            SQLiteDatabase db = getReadableDatabase();

            if (db != null) {
                Cursor cursor = db.query(getTableName(), null, fieldName + "=" + value, null, null, null, "RANDOM()");
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        object = loadObject(cursor);
                    }

                    cursor.close();
                }

                db.close();
            }
        }

        return object;
    }

    public ArrayList<T> findAllByField(String fieldName, String value) {
        ArrayList<T> result = new ArrayList<>();

        synchronized (lock) {
            SQLiteDatabase db = getReadableDatabase();
            if (db != null) {
                Cursor cursor = db.query(getTableName(), null, fieldName + "='" + value + "'", null, null, null, null);
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        while (!cursor.isAfterLast()) {
                            result.add(loadObject(cursor));
                            cursor.moveToNext();
                        }
                    }

                    cursor.close();
                }

                db.close();
            }
        }

        return result;
    }

    public ArrayList<T> findAllByField(String[] fieldNames, String[] values) {
        ArrayList<T> result = new ArrayList<>();

        synchronized (lock) {
            SQLiteDatabase db = getReadableDatabase();
            if (db != null) {
                String selection = fieldNames[0] + "=?";
                for (int i = 1; i < fieldNames.length; i++) {
                    selection = selection + " AND " + fieldNames[i] + "=?";
                }

                Cursor cursor = db.query(getTableName(), null, selection, values, null, null, null);
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        while (!cursor.isAfterLast()) {
                            result.add(loadObject(cursor));
                            cursor.moveToNext();
                        }
                    }

                    cursor.close();
                }

                db.close();
            }
        }

        return result;
    }


    public ArrayList<T> findAllByField(String[] fieldNames, String[] values, int pageNo, String order) {
        ArrayList<T> result = new ArrayList<>();

        synchronized (lock) {
            SQLiteDatabase db = getReadableDatabase();
            if (db != null) {
                String selection = fieldNames[0] + "=?";
                for (int i = 1; i < fieldNames.length; i++) {
                    selection = selection + " AND " + fieldNames[i] + "=?";
                }

                Cursor cursor = db.query(getTableName(), null, selection, values, null, null, order, String.valueOf(100 * pageNo));
                if (cursor != null) {
                    if (cursor.moveToPosition(100 * (pageNo - 1))) {
                        while (!cursor.isAfterLast()) {
                            result.add(loadObject(cursor));
                            cursor.moveToNext();
                        }
                    }

                    cursor.close();
                }

                db.close();
            }
        }

        return result;
    }

    public ArrayList<T> findAllByField(String[] fieldNames, String[] values, int pageNo, String order, String search) {
        ArrayList<T> result = new ArrayList<>();

        synchronized (lock) {
            SQLiteDatabase db = getReadableDatabase();
            if (db != null) {
                String selection = fieldNames[0] + "=?";
                for (int i = 1; i < fieldNames.length; i++) {
                    selection = selection + " AND " + fieldNames[i] + "=?";
                }
                selection = selection + " AND " + order + " LIKE '%" + search + "%'";

                Cursor cursor = db.query(getTableName(), null, selection, values, null, null, order, String.valueOf(100 * pageNo));
                if (cursor != null) {
                    if (cursor.moveToPosition(100 * (pageNo - 1))) {
                        while (!cursor.isAfterLast()) {
                            result.add(loadObject(cursor));
                            cursor.moveToNext();
                        }
                    }

                    cursor.close();
                }

                db.close();
            }
        }

        return result;
    }

    public T findFirst() {
        synchronized (lock) {
            T result = null;

            ArrayList<T> results = findWithLimit(1);
            if (results != null && results.size() > 0) {
                result = results.get(0);
            }

            return result;
        }
    }

    public long getCount() {
        synchronized (lock) {
            long count = 0;
            SQLiteDatabase db = getReadableDatabase();
            if (db != null) {
                Cursor cursor = db.rawQuery("SELECT COUNT(*) AS \"count\" FROM " + getTableName(), null);
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        count = cursor.getLong(0);
                    }

                    cursor.close();
                }

                db.close();
            }

            return count;
        }
    }

    abstract protected String getTableName();

    abstract protected T loadObject(Cursor cursor);

    abstract protected ContentValues getContentValues(T t);

    abstract protected HashMap<String, FieldType> getFields();

    abstract protected Long getId(T t);
}
