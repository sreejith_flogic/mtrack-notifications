package in.fortelogic.mtrack.common.base;

import android.content.Context;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BaseSpinnerAdapter extends ArrayAdapter<BaseSpinnerModel> {

    private final List<BaseSpinnerModel> mList = new ArrayList<>();

    public BaseSpinnerAdapter(@NonNull Context context, @NonNull BaseSpinnerModel[] objects) {
        super(context, R.layout.layout_spinner_item, objects);
        Collections.addAll(mList, objects);
    }

    public BaseSpinnerAdapter(@NonNull Context context, @NonNull List<BaseSpinnerModel> objects) {
        super(context, R.layout.layout_spinner_item, objects);
        mList.addAll(objects);
    }

    public int getIndex(BaseSpinnerModel item) {
        int index = -1;

        if (item != null) {
            for (int i = 0; i < mList.size(); i++) {
                BaseSpinnerModel model = mList.get(i);
                if (model != null && model.getId() == item.getId()) {
                    index = i;
                    break;
                }
            }
        }

        return index;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Nullable
    @Override
    public BaseSpinnerModel getItem(int position) {
        return mList.get(position);
    }
}
