package in.fortelogic.mtrack.common.base.network;

import java.util.HashMap;
import java.util.List;

/**
 * The purpose of this interface is to provide methods that provides the details
 * required by the {@link GetListAsyncTask} to run without errors.
 *
 * @param <T> template class which implements this interface
 */
public interface GetApiBaseModel<T> {
    String getURL();

    HashMap<String, Object> getArguments();

    List<T> getList(String json);
}
