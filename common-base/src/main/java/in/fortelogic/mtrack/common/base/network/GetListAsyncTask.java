package in.fortelogic.mtrack.common.base.network;

import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import java.util.HashMap;
import java.util.List;

import ch.bullfin.httpmanager.HTTPManager;
import ch.bullfin.httpmanager.Response;

/**
 * This class is responsible for pulling data from a given API using a GET call. The response
 * expected is a JSON, which is a list of objects of type T.
 *
 * The implementation requires a model that implements the interface {@link GetApiBaseModel}
 * and it's methods which provides the required details for calling the API.
 *
 * An object of the model should be passed in as an argument to the .execute() method to
 * make it available for the async task to call the implemented methods.
 *
 * @param <T> template class. this should be child of {@link GetApiBaseModel}
 */
public class GetListAsyncTask<T extends GetApiBaseModel> extends AsyncTask<T, Void, List<T>> {
    private static final String LOG_TAG = "GetListAsyncTask";
    private Callback mCallback;

    public GetListAsyncTask<T> setCallback(Callback callback) {
        mCallback = callback;
        return this;
    }

    @Override
    protected void onPreExecute() {
        if (mCallback != null) {
            mCallback.onTaskStart();
        }
    }

    @SafeVarargs
    @Override
    protected final List<T> doInBackground(T... args) {
        if (args != null && args.length > 0) {
            String apiURL = args[0].getURL();
            if (!TextUtils.isEmpty(apiURL)) {
                HTTPManager httpManager = new HTTPManager(apiURL);
                Response response;

                //noinspection unchecked
                HashMap<String, Object> params = args[0].getArguments();
                if (params == null || params.size() == 0) {
                    Log.i(LOG_TAG, "No arguments provided!");

                    response = httpManager.get();
                } else {
                    response = httpManager.get(params);
                }

                if (response != null) {
                    if (response.getStatusCode() == 200) {
                        String responseBody = response.getResponseBody();
                        if (!TextUtils.isEmpty(responseBody)) {
                            try {
                                //noinspection unchecked
                                return args[0].getList(responseBody);
                            } catch (Exception e) {
                                Log.e(LOG_TAG, e.getMessage());
                            }
                        } else {
                            Log.e(LOG_TAG, "Empty response body received from " + apiURL);
                        }
                    } else {
                        Log.e(LOG_TAG, "API error. Code: " + response.getStatusCode());
                    }
                } else {
                    Log.e(LOG_TAG, "No response received from " + apiURL);
                }
            } else {
                Log.e(LOG_TAG, "No API URL found inside the provided object");
            }
        } else {
            Log.e(LOG_TAG, "No valid model provided with URL and params.");
        }

        return null;
    }

    @Override
    protected void onPostExecute(List<T> results) {
        if (mCallback != null) {
            //noinspection unchecked
            mCallback.onTaskComplete(results);
        }
    }

    public interface Callback<T> {
        void onTaskStart();

        void onTaskComplete(List<T> results);
    }
}
