package in.fortelogic.mtrack.common.base.network;

import java.util.HashMap;

public interface GetDetailsBaseModel<T> {
    String getObjectURL();

    HashMap<String, Object> getObjectArguments();

    T getObjectDetails(String json);
}