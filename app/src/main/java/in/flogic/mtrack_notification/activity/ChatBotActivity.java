package in.flogic.mtrack_notification.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.rahulrvp.android_utils.EditTextUtils;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import in.flogic.mtrack_notification.R;
import in.flogic.mtrack_notification.adapter.AnnouncementAdapter;
import in.flogic.mtrack_notification.model.Message;
import in.fortelogic.mtrack.common.base.MTrackActivity;

public class ChatBotActivity extends MTrackActivity {

    private AnnouncementAdapter mAdapter;
    private RecyclerView mChatRecycler;
    private EditText mQueryEdittext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatbot);

        mChatRecycler = findViewById(R.id.chat_recycler);
        mAdapter = new AnnouncementAdapter();
        mQueryEdittext = findViewById(R.id.query_edit_text);

        if (mChatRecycler != null) {
            mChatRecycler.setLayoutManager(new LinearLayoutManager(mContext));
            mChatRecycler.setAdapter(mAdapter);
        }

        fetchChat();
    }

    private void fetchChat() {

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("chat_bot").child("user_123");
        myRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Message message = dataSnapshot.getValue(Message.class);
                mAdapter.addMessages(message);
                mChatRecycler.smoothScrollToPosition(mAdapter.getItemCount() + 1);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    public void onChatWithBotClicked(View view) {
        String queryString = EditTextUtils.getText(mQueryEdittext);
        if (! TextUtils.isEmpty(queryString)) {
            Message query = new Message();
            query.setContent(queryString);
            query.setTime(System.currentTimeMillis());
            query.setUserId(1);

            Message reply = new Message();
            reply.setTime(System.currentTimeMillis());
            reply.setContent("This is the reply from chat bot ");

            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference myRef = database.getReference("chat_bot").child("user_123");
            String key = myRef.push().getKey();
            myRef.child(key + "q").setValue(query);
            myRef.child(key + "r").setValue(reply);
        }
    }
}
