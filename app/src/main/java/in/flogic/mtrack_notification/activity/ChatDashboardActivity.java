package in.flogic.mtrack_notification.activity;

import android.os.Bundle;
import android.os.PersistableBundle;

import androidx.annotation.Nullable;

import co.chatsdk.ui.main.MainAppBarActivity;

public class ChatDashboardActivity extends MainAppBarActivity {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
