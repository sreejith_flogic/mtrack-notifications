package in.flogic.mtrack_notification.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import co.chatsdk.core.session.ChatSDK;
import in.flogic.mtrack_notification.R;
import in.flogic.mtrack_notification.adapter.AnnouncementAdapter;
import in.flogic.mtrack_notification.database.AnnouncementTable;
import in.flogic.mtrack_notification.model.Message;
import in.fortelogic.mtrack.common.base.MTrackActivity;

public class AnnouncementListActivity extends MTrackActivity {

    private AnnouncementAdapter mAdapter;
    private RecyclerView mNotificationRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_announcement_list);

        mNotificationRecyclerView = findViewById(R.id.announcement_recycler);
        mAdapter = new AnnouncementAdapter();

        if (mNotificationRecyclerView != null) {
            mNotificationRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
            mNotificationRecyclerView.setAdapter(mAdapter);

            fetchAnouncements("subject1");
            mAdapter.setData(AnnouncementTable.getInstance(mContext).findAll());
            mNotificationRecyclerView.smoothScrollToPosition(mAdapter.getItemCount());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_layout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.chat:
                ChatSDK.ui().setMainActivity(ChatDashboardActivity.class);
                Intent intent = new Intent(this, ChatSDK.ui().getMainActivity());
                startActivity(intent);
                return true;
            case R.id.chat_bot:
                startActivity(new Intent(mContext, ChatBotActivity.class));
                return true;

            case R.id.notifications:
                startActivity(new Intent(mContext, NotificationListActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void fetchAnouncements(String topic) {

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("announcement").child(topic);
        myRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Message message = dataSnapshot.getValue(Message.class);
                AnnouncementTable.getInstance(mContext).insert(message);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
