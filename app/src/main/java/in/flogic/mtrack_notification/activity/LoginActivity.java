package in.flogic.mtrack_notification.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import co.chatsdk.core.session.ChatSDK;
import co.chatsdk.core.types.AccountDetails;
import in.fortelogic.mtrack.common.login.MTrackLoginActivity;
import in.fortelogic.mtrack.common.login.User;
import io.reactivex.android.schedulers.AndroidSchedulers;

import static co.chatsdk.core.types.AccountDetails.signUp;
import static co.chatsdk.core.types.AccountDetails.username;

public class LoginActivity extends MTrackLoginActivity {

    private static final String LOG_TAG = "MTRACK_NOTIFICATION";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ChatSDK.ui().setSplashScreenActivity(LoginActivity.class);
        if (ChatSDK.currentUser() != null) {
            launchNextPage();
        } else {
            User.logout(mContext);
        }
    }

    @Override
    protected Class getNextPage() {
        return AnnouncementListActivity.class;
    }

    @Override
    protected String getInterfaceName() {
        return "tagging";
    }

    @Override
    protected void onLoginEnd() {
        super.onLoginEnd();
        createChatUser();
    }

    @Override
    protected void onLoginBegin() {
        super.onLoginBegin();
        showProgress();
    }

    private void createChatUser() {
        showProgress();
        User user = User.getInstance(mContext);
        AccountDetails details = signUp(user.getUsername(), "flat2041");
        ChatSDK.auth().authenticate(details)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccess, this::onSignupFailed);

    }

    private void signinChatUser() {
        showProgress();
        User user = User.getInstance(mContext);
        AccountDetails details = username(user.getUsername(), "flat2041");
        ChatSDK.auth().authenticate(details)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccess, this::onLoginFailed);

    }

    private void onSuccess() {
        Log.d(LOG_TAG, "We're in!");

        ChatSDK.currentUser().setName(User.getInstance(mContext).getUsername());
        launchNextPage();
        hideProgress();
        finish();
    }

    private void onLoginFailed(Throwable throwable) {
        Log.d(LOG_TAG, throwable.toString());
        hideProgress();
    }

    private void onSignupFailed(Throwable throwable) {
        Log.d(LOG_TAG, throwable.toString());
        hideProgress();
        signinChatUser();
    }
}
