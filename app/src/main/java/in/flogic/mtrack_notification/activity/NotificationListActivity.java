package in.flogic.mtrack_notification.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import co.chatsdk.core.session.ChatSDK;
import in.flogic.mtrack_notification.adapter.NotificationsAdapter;
import in.flogic.mtrack_notification.R;
import in.fortelogic.mtrack.common.base.MTrackActivity;

public class NotificationListActivity extends MTrackActivity {

    private NotificationsAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_list);

        RecyclerView notificationRecyclerView = findViewById(R.id.notifications_recycler);
        mAdapter = new NotificationsAdapter();

        if (notificationRecyclerView != null) {
            notificationRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
            notificationRecyclerView.setAdapter(mAdapter);
        }
    }


    public void onChatClicked(View view) {
        ChatSDK.ui().setMainActivity(ChatDashboardActivity.class);
        Intent intent = new Intent(this, ChatSDK.ui().getMainActivity());
        startActivity(intent);
    }
}
