package in.flogic.mtrack_notification.framework;

import android.app.Application;

import co.chatsdk.core.error.ChatSDKException;
import co.chatsdk.core.session.ChatSDK;
import co.chatsdk.core.session.Configuration;
import co.chatsdk.firebase.FirebaseNetworkAdapter;
import co.chatsdk.firebase.file_storage.FirebaseFileStorageModule;
import co.chatsdk.firebase.push.FirebasePushModule;
import co.chatsdk.firebase.ui.FirebaseUIModule;
import co.chatsdk.ui.manager.BaseInterfaceAdapter;
import in.flogic.mtrack_notification.R;

public class MtrackApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        initChatSdk();
    }

    private void initChatSdk() {
        try {
            Configuration.Builder builder = new Configuration.Builder(this);
            builder.firebaseRootPath("mtrack_chat_test");
            builder.defaultName("Mtrack User");
            builder.setClientPushEnabled(true);
            builder.locationMessagesEnabled(false);
            builder.onlySendPushToOfflineUsers(false);
            builder.logoDrawableResourceID(R.mipmap.ic_launcher);
            ChatSDK.initialize(builder.build(), new FirebaseNetworkAdapter(), new BaseInterfaceAdapter(this));

            FirebaseFileStorageModule.activate();
            FirebaseUIModule.activate();
            FirebasePushModule.activate();

        } catch (ChatSDKException e) {
            e.printStackTrace();
        }
    }
}
