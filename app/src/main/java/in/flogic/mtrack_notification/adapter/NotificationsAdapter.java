package in.flogic.mtrack_notification.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.github.rahulrvp.android_utils.TextViewUtils;

import java.util.ArrayList;
import java.util.List;

import in.flogic.mtrack_notification.R;
import in.fortelogic.mtrack.common.ui.ESSemiBold;

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.ViewHolder> {

    private List<String> mNotifications = new ArrayList<>();

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rootView = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.notification_item, parent, false);
        return new ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String message = "Notification message will come here";
        String title = "Title";
        holder.fillValues(message, title, position);
    }

    @Override
    public int getItemCount() {
        return 12;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView messageTV;
        private TextView titleTV;

        ViewHolder(View itemView) {
            super(itemView);

            messageTV = itemView.findViewById(R.id.notification_message);
            titleTV = itemView.findViewById(R.id.notification_title);

            ESSemiBold.getInstance().apply(titleTV);
        }

        void fillValues(String message, String title, final int position) {
            TextViewUtils.setText(titleTV, title);
            TextViewUtils.setText(messageTV, message);
        }
    }
}
