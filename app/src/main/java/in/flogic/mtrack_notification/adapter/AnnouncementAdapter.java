package in.flogic.mtrack_notification.adapter;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.github.rahulrvp.android_utils.TextViewUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import in.flogic.mtrack_notification.ContentType;
import in.flogic.mtrack_notification.R;
import in.flogic.mtrack_notification.model.Message;
import in.fortelogic.mtrack.common.ui.ESSemiBold;
import in.fortelogic.mtrack.common.util.Utils;

public class AnnouncementAdapter extends RecyclerView.Adapter<AnnouncementAdapter.ViewHolder> {

    private List<Message> mAnnouncements;

    public AnnouncementAdapter() {
        this.mAnnouncements = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rootView = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.announcement_item, parent, false);
        return new ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Message message = mAnnouncements.get(position);
        holder.fillValues(message, position);
    }

    @Override
    public int getItemCount() {
        return mAnnouncements.size();
    }

    public void setData(ArrayList<Message> messages) {
        mAnnouncements = messages;
        notifyDataSetChanged();
    }

    public void addMessages(Message message) {
        mAnnouncements.add(message);
        notifyItemChanged(mAnnouncements.size() - 1);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView messageTV;
        private TextView outGoingMessageTV;
        private TextView titleTV;
        private TextView messageTime;
        private TextView outgoingMessageTime;
        private ImageView chatImage;
        private ImageView playButton;
        private LinearLayout incomingLayout;
        private LinearLayout outgoingLayout;

        ViewHolder(View itemView) {
            super(itemView);

            messageTV = itemView.findViewById(R.id.notification_message);
            titleTV = itemView.findViewById(R.id.notification_title);
            chatImage = itemView.findViewById(R.id.chat_image);
            playButton = itemView.findViewById(R.id.play_image);
            outGoingMessageTV = itemView.findViewById(R.id.outgoing_message);
            incomingLayout = itemView.findViewById(R.id.incoming_layout);
            outgoingLayout = itemView.findViewById(R.id.out_going_layout);
            messageTime = itemView.findViewById(R.id.message_time);
            outgoingMessageTime = itemView.findViewById(R.id.outgoing_message_time);
            ESSemiBold.getInstance().apply(titleTV);

            // Random textColor for username
            Random rnd = new Random();
            int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
            titleTV.setTextColor(color);
        }

        void fillValues(Message message, final int position) {
            if (message != null) {

                if (message.getUserId() == 1) {

                    // outgoing message
                    outgoingLayout.setVisibility(View.VISIBLE);
                    incomingLayout.setVisibility(View.GONE);
                    TextViewUtils.setText(outGoingMessageTV, message.getContent());
                    TextViewUtils.setText(outgoingMessageTime, Utils.formatTime(message.getTime()));
                    itemView.setOnClickListener(null);
                } else {

                    // Incoming message
                    outgoingLayout.setVisibility(View.GONE);
                    incomingLayout.setVisibility(View.VISIBLE);

                    if (!TextUtils.isEmpty(message.getTitle())) {
                        titleTV.setVisibility(View.VISIBLE);
                        TextViewUtils.setText(titleTV, message.getTitle());
                    } else {
                        titleTV.setVisibility(View.GONE);
                    }

                    TextViewUtils.setText(messageTV, message.getContent());
                    TextViewUtils.setText(messageTime, Utils.formatTime(message.getTime()));

                    if ((message.getContentType() == ContentType.IMAGE
                            || message.getContentType() == ContentType.VIDEO)
                            && !TextUtils.isEmpty(message.getUrl())) {

                        chatImage.setVisibility(View.VISIBLE);
                        Glide.with(chatImage.getContext()).load(message.getUrl()).into(chatImage);

                        if (message.getContentType() == ContentType.VIDEO) {
                            playButton.setVisibility(View.VISIBLE);
                        } else {
                            playButton.setVisibility(View.GONE);
                        }

                        itemView.setOnClickListener(v -> {

                            if (message.getContentType() == ContentType.VIDEO) {
                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(message.getUrl()));
                                intent.setDataAndType(Uri.parse(message.getUrl()), "video/mp4");
                                itemView.getContext().startActivity(intent);
                            } else if (message.getContentType() == ContentType.IMAGE) {
                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_VIEW);
                                intent.setDataAndType(Uri.parse(message.getUrl()), "image/*");
                                itemView.getContext().startActivity(intent);
                            }
                        });

                    } else {
                        chatImage.setVisibility(View.GONE);
                        playButton.setVisibility(View.GONE);
                        itemView.setOnClickListener(null);
                    }
                }

            }
        }
    }
}
