package in.flogic.mtrack_notification.model;

import android.text.TextUtils;

import in.flogic.mtrack_notification.ContentType;
import in.fortelogic.mtrack.common.util.Utils;

public class Message {
    private long id;
    private long time;
    private long userId;
    private String title;
    private String content;
    private ContentType contentType;
    private String url;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public ContentType getContentType() {
        return contentType;
    }

    public void setContentType(ContentType contentType) {
        this.contentType = contentType;
    }

    public void setContentTypeFromString(String contentType) {
        if (! TextUtils.isEmpty(contentType)) {
            this.contentType = ContentType.valueOf(contentType);
        }
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }
}
