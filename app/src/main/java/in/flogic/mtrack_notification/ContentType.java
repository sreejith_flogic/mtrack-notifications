package in.flogic.mtrack_notification;

public enum ContentType {
    TEXT,
    AUDIO,
    VIDEO,
    IMAGE
}
