package in.flogic.mtrack_notification.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.HashMap;

import in.flogic.mtrack_notification.model.Message;

public class AnnouncementTable extends CommonSqliteTable<Message> {
    static final String TABLE_NAME = "announcement_table";

    private static final String FIELD_ID = "id";
    private static final String FIELD_USER_ID = "user_id";
    private static final String FIELD_TITLE = "title";
    private static final String FIELD_CONTENT = "content";
    private static final String FIELD_TYPE = "type";
    private static final String FIELD_TIMESTAMP = "timestamp";
    ;
    private static final String FIELD_URL = "url";

    private static AnnouncementTable sInstance;

    public AnnouncementTable(Context context) {
        super(context);
    }

    public static AnnouncementTable getInstance(Context context) {
        synchronized (lock) {
            if (sInstance == null) {
                sInstance = new AnnouncementTable(context);
            }

            return sInstance;
        }
    }

    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Override
    protected Message loadObject(Cursor cursor) {
        Message bundle = new Message();

        if (cursor != null) {
            bundle.setId(cursor.getInt(cursor.getColumnIndex(FIELD_ID)));
            bundle.setUserId(cursor.getInt(cursor.getColumnIndex(FIELD_USER_ID)));
            bundle.setTitle(cursor.getString(cursor.getColumnIndex(FIELD_TITLE)));
            bundle.setContent(cursor.getString(cursor.getColumnIndex(FIELD_CONTENT)));
            bundle.setTime(cursor.getLong(cursor.getColumnIndex(FIELD_TIMESTAMP)));
            bundle.setUrl(cursor.getString(cursor.getColumnIndex(FIELD_URL)));
            bundle.setContentTypeFromString(cursor.getString(cursor.getColumnIndex(FIELD_TYPE)));
        }
        return bundle;
    }

    @Override
    protected ContentValues getContentValues(Message productBundle) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(FIELD_USER_ID, productBundle.getUserId());
        contentValues.put(FIELD_TITLE, productBundle.getTitle());
        contentValues.put(FIELD_CONTENT, productBundle.getContent());
        contentValues.put(FIELD_URL, productBundle.getUrl());
        contentValues.put(FIELD_TIMESTAMP, productBundle.getTime());
        if (productBundle.getContentType() != null) {
            contentValues.put(FIELD_TYPE, productBundle.getContentType().toString());
        }
        return contentValues;
    }

    @Override
    protected HashMap<String, FieldType> getFields() {
        HashMap<String, FieldType> fieldTypeHashMap = new HashMap<>();
        fieldTypeHashMap.put(FIELD_ID, FieldType.Autoincrement);
        fieldTypeHashMap.put(FIELD_USER_ID, FieldType.Long);
        fieldTypeHashMap.put(FIELD_TITLE, FieldType.String);
        fieldTypeHashMap.put(FIELD_CONTENT, FieldType.String);
        fieldTypeHashMap.put(FIELD_TYPE, FieldType.String);
        fieldTypeHashMap.put(FIELD_URL, FieldType.String);
        fieldTypeHashMap.put(FIELD_TIMESTAMP, FieldType.Long);
        return fieldTypeHashMap;
    }

    @Override
    protected Long getId(Message productBundle) {
        return productBundle.getId();
    }

}
