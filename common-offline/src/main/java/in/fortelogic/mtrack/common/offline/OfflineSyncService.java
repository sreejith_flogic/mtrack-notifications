package in.fortelogic.mtrack.common.offline;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.Nullable;

import java.util.List;

public abstract class OfflineSyncService<T> extends IntentService {

    public static final String EXTRA_STATUS = "offline-sync-status";
    public static final String EXTRA_MESSAGE = "offline-sync-message";

    public static final int STATUS_STARTED = 0;
    public static final int STATUS_COMPLETE = 1;
    public static final int STATUS_UPDATE = 2;

    private static final String LOG_TAG = "Offline Sync";

    public OfflineSyncService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        // notify the start
        onSyncStart();

        boolean isComplete = true;

        List<T> pendingItems = getAllPendingItems();
        if (pendingItems != null) {
            int pendingCount = pendingItems.size();

            for (T pendingItem : pendingItems) {
                boolean isSuccess = callWebService(pendingItem);

                if (isSuccess) {
                    --pendingCount;

                    onApiSuccess(pendingItem);

                    // notify the progress
                    onPendingCountChange(pendingCount);
                } else {
                    isComplete = false;

                    onApiFailure(pendingItem);
                }
            }
        }

        // notify the end
        onSyncStop(isComplete);
    }

    protected void sendStartedMsg() {
        sendBroadcast(STATUS_STARTED, "");
    }

    protected void sendStoppedMsg() {
        sendBroadcast(STATUS_COMPLETE, "");
    }

    protected void sendCountUpdateMsg(long count) {
        sendBroadcast(STATUS_UPDATE, String.valueOf(count));
    }

    /**
     * This method is a way to indicate the events of this service.
     * <p>
     * Such as, sync started, ended and updated.
     * <p>
     * Name of action is expected as the combination of pkgname and classname. Or something unique.
     * <p>
     * Ex: if class name is BilletSyncService, then the broadcasts will be sent under the action
     * name app.package.name.BilletSyncService
     *
     * @param status  status of the service
     * @param message message for current status
     */
    private void sendBroadcast(int status, String message) {
        String action = getAction();
        Intent messageIntent = new Intent(action);
        messageIntent.putExtra(EXTRA_STATUS, status);
        messageIntent.putExtra(EXTRA_MESSAGE, message);
        sendBroadcast(messageIntent);

        Log.d(LOG_TAG, "Broadcast Action: " + action);
        Log.d(LOG_TAG, "Broadcast Extra Status: " + status);
        Log.d(LOG_TAG, "Broadcast Extra Message: " + message);
    }

    protected abstract String getAction();

    protected abstract boolean callWebService(T item);

    protected abstract List<T> getAllPendingItems();

    protected abstract void onApiSuccess(T item);

    protected abstract void onApiFailure(T item);

    protected abstract void onSyncStart();

    protected abstract void onSyncStop(boolean isComplete);

    protected abstract void onPendingCountChange(long pendingCount);
}
