package in.fortelogic.mtrack.common.ui;

import com.github.rahulrvp.android_utils.BaseFont;


/**
 * @author Rahul Raveendran V P
 *         Created on 26/03/18 @ 5:52 PM
 *         https://github.com/rahulrvp
 */


public class ESBold extends BaseFont {

    private static ESBold instance;

    public static ESBold getInstance() {
        if (instance == null) {
            instance = new ESBold();
        }

        return instance;
    }

    private ESBold() {

    }

    @Override
    protected String getPath() {
        return "fonts/EncodeSans-Bold.ttf";
    }
}
