package in.fortelogic.mtrack.common.ui;

import com.github.rahulrvp.android_utils.BaseFont;


/**
 * @author Rahul Raveendran V P
 *         Created on 26/03/18 @ 5:52 PM
 *         https://github.com/rahulrvp
 */


public class ESRegular extends BaseFont {

    private static ESRegular instance;

    public static ESRegular getInstance() {
        if (instance == null) {
            instance = new ESRegular();
        }

        return instance;
    }

    private ESRegular() {

    }

    @Override
    protected String getPath() {
        return "fonts/EncodeSans-Regular.ttf";
    }
}
