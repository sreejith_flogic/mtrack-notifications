package in.fortelogic.mtrack.common.ui;

import com.github.rahulrvp.android_utils.BaseFont;


/**
 * @author Rahul Raveendran V P
 *         Created on 26/03/18 @ 5:52 PM
 *         https://github.com/rahulrvp
 */


public class ESMedium extends BaseFont {

    private static ESMedium instance;

    public static ESMedium getInstance() {
        if (instance == null) {
            instance = new ESMedium();
        }

        return instance;
    }

    private ESMedium() {

    }

    @Override
    protected String getPath() {
        return "fonts/EncodeSans-Medium.ttf";
    }
}
