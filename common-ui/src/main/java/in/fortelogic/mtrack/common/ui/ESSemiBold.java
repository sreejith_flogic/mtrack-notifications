package in.fortelogic.mtrack.common.ui;

import com.github.rahulrvp.android_utils.BaseFont;


/**
 * @author Rahul Raveendran V P
 *         Created on 26/03/18 @ 5:52 PM
 *         https://github.com/rahulrvp
 */


public class ESSemiBold extends BaseFont {

    private static ESSemiBold instance;

    public static ESSemiBold getInstance() {
        if (instance == null) {
            instance = new ESSemiBold();
        }

        return instance;
    }

    private ESSemiBold() {

    }


    @Override
    protected String getPath() {
        return "fonts/EncodeSans-SemiBold.ttf";
    }
}
